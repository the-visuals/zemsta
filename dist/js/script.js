/* global monogatari */

// Define the messages used in the game.
monogatari.action ('message').messages ({
	'config': {
		title: 'Konfiguracja hiperparametrów...',
		body: `
			<p>Drogi Graczu,<br /> za chwilę zostanie ci zadanych kilka pytań.<br />
			Twoje odpowiedzi wpłyną na przebieg rozgrywki, zatem wybieraj uważnie. </p>
		`
	},
	'postConfig': {
		title: 'Konfiguracja zakończona!',
		body: `
			<p>Twoje odpowiedzi zostały zapisane. Pamiętaj, że w każdej chwili możesz wrócić i je zmienić. Wpłynie to jednak na przebieg rozgrywki. Będzie również jednoznaczne z porzuceniem relacji, którą udało ci się zbudować. </p>
		`
	}
});

// Define the notifications used in the game
monogatari.action ('notification').notifications ({
	'Welcome': {
		title: 'Welcome',
		body: 'This is the Monogatari VN Engine',
		icon: ''
	}
});

// Credits of the people involved in the creation of this awesome game
monogatari.configuration ('credits', {

});

// Define the Particles JS Configurations used in the game
monogatari.action ('particles').particles ({

});

// Define the images that will be available on your game's image gallery
monogatari.assets ('gallery', {

});

// Define the music used in the game.
monogatari.assets ('music', {
	// deepBlue: 'bensound-deepblue.mp3',
	// acousticBreeze: 'bensound-acousticbreeze.mp3',
	// scifi: 'bensound-scifi.mp3',
	// memories: 'bensound-memories.mp3',
	// sadDay: 'bensound-sadday.mp3',
	// tomorrow: 'bensound-tomorrow.mp3',
	// enigmatic: 'bensound-enigmatic.mp3',
	// psychedelic: 'bensound-psychedelic.mp3',
	// retrosoul: 'bensound-retrosoul.mp3',
	// hipjazz: 'bensound-hipjazz.mp3',
	// hipjazz_EQ: 'hip jazz_EQ.mp3',
	// retrosoul_EQ: 'retro soul_EQ.mp3',
	// Magical_Dirt: 'Magical_Dirt.mp3',
	// White_Hats: 'White_Hats.mp3',
	// creepy: 'bensound-creepy.mp3',
	// insects: 'insects.mp3',
	// evolution: 'bensound-evolution.mp3',
	// november: 'bensound-november.mp3',
	// piano_moment: 'bensound-pianomoment.mp3',
	// We_ll_Meet_Again: 'We_ll_Meet_Again.mp3',
	// ending: 'ending.mp3',
	lute1: 'lute1.mp3',
	lute2: 'lute2.mp3',
	lute3: 'lute3.mp3',
	lute4: 'lute4.mp3',
	house: 'house.mp3',
	finalLabor: 'final-labor.mp3',
	hero: 'bensound-birthofahero.mp3',
	elegy: 'Elegy - Wayne Jones.mp3',
	dungeon: 'dungeon.mp3'
});

// Define the voice files used in the game.
monogatari.assets ('voices', {});

// Define the sounds used in the game.
monogatari.assets ('sounds', {
	// loudKnock: 'loudKnock.mp3',
	// louderKnock: 'louderKnock.mp3',
	// softKnock: 'softKnock.mp3',
	// cameraShutter: 'cameraShutter.mp3',
	// notification: 'notification.mp3',
	// keyboardLogin: 'keyboardLogin.mp3',
	// slammingTheDoor: 'slammingTheDoor.mp3',
	// openingTheDoor: 'openingTheDoor.mp3',
	// slowDoorOpening: 'slowDoorOpen.mp3',
	// schoolBell: 'schoolBell.mp3',
	// thump: 'thump.mp3',
	// pulling: 'pulling.mp3',
	// laughTrack: 'laughTrack.mp3',
	// dentalDrill: 'dentalDrill.mp3',
	// dentalDrillLong: 'dentalDrillLong.mp3',
	// ruriri_007: 'ruriri_007.mp3',
	// Glass_Windows_Breaking: 'Glass_Windows_Breaking.mp3',
	bodyfall: 'body fall.mp3',
	// bitpowerup: '8-bit-powerup.mp3',
	// screeching: 'screeching.mp3',
	// doorhandle: 'doorhandle.mp3',
	// schoolChildren: 'schoolChildren.mp3',
	// hospitalCorridor: 'hospitalCorridor.mp3',
	// dentalOffice: 'dentalOffice.mp3',
	// slidingDoor: 'slidingDoor.mp3',
	// 'blizzard': 'blizzard.mp3',
	// 'typing1': 'typing1.mp3',
	// 'typing2': 'typing2.mp3',
	// 'typing3': 'typing3.mp3',
	// 'typing4': 'typing4.mp3',
	// 'beach_sea': 'beach_sea.mp3',
	// 'street1' : 'street1.mp3',
	// 'street2' : 'street2.mp3',
	// 'beep' : 'beep.mp3',
	tavern: 'tavern.mp3',
	'footsteps': 'footsteps.mp3',
	'doorOpenSqueek': 'doorOpenSqueek.mp3',
	teleport: 'teleport.mp3',
	demon: 'demon.mp3',
	party: 'party.mp3',
	shop_bell: 'shop_bell.mp3',
	ruriri_179: 'ruriri_179.mp3',
});

// Define the videos used in the game.
monogatari.assets ('videos', {

});

// Define the images used in the game.
monogatari.assets ('images', {

});

function canUseWebP () {
	var elem = document.createElement('canvas');
	if (elem.getContext && elem.getContext('2d')) {
		return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
	}
	return false;
}
var webPSupported = canUseWebP();

// Define the backgrounds for each scene.
monogatari.assets ('scenes', {
	// bathroom: webPSupported ? 'bathroom.webp' : 'bathroom.png',
	// beachDaySimple: webPSupported ? 'beach_day_simple.webp' : 'beach_day_simple.png',
	// beachDayUmbrella: webPSupported ? 'beach_day_umbrella.webp' : 'beach_day_umbrella.png',
	// beachSunsetSimple: webPSupported ? 'beach_sunset_simple.webp' : 'beach_sunset_simple.png',
	// bedroomNight: webPSupported ? 'bedroom_night.webp' : 'bedroom_night.png',
	// bedroom: webPSupported ? 'bedroom.webp' : 'bedroom.png',
	// busStop: webPSupported ? 'bus_stop.webp' : 'bus_stop.png',
	// cafe: webPSupported ? 'cafe.webp' : 'cafe.png',
	// classroomEvening: webPSupported ? 'classroom_evening.webp' : 'classroom_evening.png',
	// classroom: webPSupported ? 'classroom.webp' : 'classroom.png',
	// classroomNight: 'classroom_night.png',
	// hallwayHouse: webPSupported ? 'hallway_house.webp' : 'hallway_house.jpg',
	// hallwayHouseNight: webPSupported ? 'hallway_house_night.webp' : 'hallway_house_night.png',
	// hallwaySchool: webPSupported ? 'hallway_school.webp' : 'hallway_school.png',
	// hallwaySchoolNight: 'hallway_school_night.png',
	// hallwaySchoolDarker30: 'hallway_school_darker_30.png',
	// hallwaySchoolDarker50: 'hallway_school_darker_50.png',
	// hallwaySchoolDarker60: 'hallway_school_darker_60.png',
	// hallwaySchoolDarker70: 'hallway_school_darker_70.png',
	// hallwaySchoolDarker75: 'hallway_school_darker_75.png',
	// hallwaySchoolDarker80: 'hallway_school_darker_80.png',
	// hallwaySchoolDarker90: 'hallway_school_darker_90.png',
	// hallway: webPSupported ? 'hallway.webp' : 'hallway.jpg',
	// hallway2: webPSupported ? 'hallway2.webp' : 'hallway2.jpg',
	// hallwayNight: webPSupported ? 'hallway_night.webp' : 'hallway_night.jpg',
	// housesEvening: webPSupported ? 'houses_evening.webp' : 'houses_evening.png',
	// housesNight: webPSupported ? 'houses_night.webp' : 'houses_night.png',
	// houses: webPSupported ? 'houses.webp' : 'houses.png',
	// japaneseSetting: webPSupported ? 'japanese_setting.webp' : 'japanese_setting.png',
	// kitchen: webPSupported ? 'kitchen.webp' : 'kitchen.png',
	// livingRoom: webPSupported ? 'living_room.webp' : 'living_room.png',
	// livingRoomNight: webPSupported ? 'living_room_night.webp' : 'living_room_night.png',
	// moonLinkFence: webPSupported ? 'moon_link_fence.webp' : 'moon_link_fence.png',
	// moonOverField: webPSupported ? 'moon_over_field.webp' : 'moon_over_field.png',
	// moonPark: webPSupported ? 'moon_park.webp' : 'moon_park.png',
	// moon: webPSupported ? 'moon.webp' : 'moon.png',
	// park: webPSupported ? 'park.webp' : 'park.png',
	// schoolCafeteria: webPSupported ? 'school_cafeteria.webp' : 'school_cafeteria.png',
	// schoolEntranceEvening: webPSupported ? 'school_entrance_evening.webp' : 'school_entrance_evening.png',
	// schoolEntrance: webPSupported ? 'school_entrance.webp' : 'school_entrance.png',
	// schoolRooftop: webPSupported ? 'school_rooftop.webp' : 'school_rooftop.png',
	// stairs: webPSupported ? 'stairs.webp' : 'stairs.png',
	// kitchenNight: webPSupported ? 'kitchen_night.webp' : 'kitchen_night.png',
	// end: webPSupported ? 'end.webp' : 'end.png',
	// Visuals 02 starts here
	// traditionalPavilion: 'traditional_pavilion.png',
	// traditionalPavilionNight: 'traditional_pavilion_night.png',
	// street1: 'street_1.png',
	// street_1Night: 'street_1Night.png',
	// street2: 'street_2.png',
	// shop1: 'shop_1.png',
	// resto: 'resto.png',
	// nightDistrict1: 'night_district_1.png',
	// nightDistrict2: 'night_district_2.png',
	// mountFuji: 'mount_fuji.png',
	// japaneseDinner: 'japanese_dinner.png',
	// houseNight: 'houseNight.png',
	// houseOld: 'house_old.png',
	// houseOldNight: 'house_old_night.png',
	// cityView: 'city_view.png',
	time: 'time.jpg',
	// dentalOffice: 'dental_office.png',
	// beach_back: 'beach_back.png',
	// city: 'city.png',
	// sea_night: 'sea_night.png',
	// street_cafe: 'street_cafe.png',
	// beach_road: 'beach_road.png',
	// bunker: 'bunker.jpg',
	arcade_fancy_day: 'arcade_fancy_day.png',
	arcade_fancy_evening: 'arcade_fancy_evening.png',
	arcade_fancy_night_lights: 'arcade_fancy_night_lights.png',
	arcade_fancy_night_no_lights: 'arcade_fancy_night_no_lights.png',
	arcade_simple_day: 'arcade_simple_day.png',
	arcade_simple_evening: 'arcade_simple_evening.png',
	arcade_simple_night: 'arcade_simple_night.png',
	archery_night: 'archery_night.png',
	archery_training: 'archery_training.png',
	castle_day: 'castle_day.png',
	castle_evening: 'castle_evening.png',
	castle_night_dark: 'castle_night_dark.png',
	castle_night2: 'castle_night2.png',
	potion_shop: 'potion_shop.png',
	prison_cell_day: 'prison_cell_day.png',
	prison_cell_night: 'prison_cell_night.png',
	well_day: 'well_day.png',
	well_evening: 'well_evening.png',
	well_night: 'well_night.png',
	well_night2: 'well_night2.png',
	tavern: 'tavern.png',
	AntiqueBakery1: 'AntiqueBakery1.png',
	AntiqueBakery2: 'AntiqueBakery2.png',
	AntiqueBakery3: 'AntiqueBakery3.png',
	AntiqueBakery4: 'AntiqueBakery4.png',
	festival: 'festival.png',
});


// Define the Characters
monogatari.characters ({
	g: {
		name: 'Strażnik',
		color: '#ff6666',
		directory: 'guard',
		sprites: {
			guardAngry: 'guardAngry.png',
			guardVeryAngry: 'guardVeryAngry.png',
			guardIrritated: 'guardIrritated.png',
			guardNeutral: 'guardNeutral.png',
		}
	},
	b: {
		name: 'Piekarzowa',
		color: '#6098d1',
		directory: 'baker',
		sprites: {
			piekarzowaSmile: 'piekarzowa_smile.png',
			piekarzowaSurprised: 'piekarzowa_surprised.png',
			piekarzowaWorried: 'piekarzowa_worried.png',
		}
	},
	mu: {
		name: '???',
		color: '#ababab',
	},
	m: {
		name: 'Czarodziejka',
		color: '#7d51f5',
		directory: 'magician',
		sprites: {
			magicianSad: 'magicianSad.png',// już jest
			magicianTriumphant: 'magicianTriumphant.png',
			magicianCourageous: 'magicianCourageous.png', //zagrzewa się do walki, płomienie w oczach i te klimaty
			magicianScared: 'magicianScared.png', // scared/shocked
			magicianCrying: 'magicianCrying.png', // sad tylko ze łzami
			magicianAngry: 'magicianAngry.png',
			magicianAngryGlow: 'magicianAngryGlow.png',
			magicianTriumphantGlow: 'magicianTriumphantGlow.png',
			magicianSadGlow: 'magicianSadGlow.png',
		}
	},
	s: {
		name: 'Szur',
		color: '#ffae66',
		directory: 'szur',
		sprites: {
			szurHappy: 'szurHappy.png', // już jest
			szurWorried: 'szurWorried.png', // już jest
			szurAngry: 'szurAngry.png',
			szurTriumphant: 'szurTriumphant.png',
			szurHappyBezAmulet: 'szurHappyBezAmulet.png', // już jest
			szurWorriedBezAmulet: 'szurWorriedBezAmulet.png', // już jest
			szurAngryBezAmulet: 'szurAngryBezAmulet.png',
			szurTriumphantBezAmulet: 'szurTriumphantBezAmulet.png',
		}
	},
	a: {
		name: 'Łuczniczka',
		color: '#21d170',
		directory: 'archer',
		sprites: {
			luczniczkaExcited: 'luczniczkaExcited.png',
			luczniczkaSuspicious: 'luczniczkaSuspicious.png',
			luczniczkaDepressed: 'luczniczkaDepressed.png',
			luczniczkaOutraged: 'luczniczkaOutraged.png',
		}
	},
	c: {
		name: 'main character',
		color: '#68C5D9',
		directory: 'aloes',
		sprites: {}
	},
	wu: {
		name: '',
		color: '#f90d00',
	},
	k1: {
		name: 'Klient karczmy 1',
		color: '#d4d4d4',
	},
	k2: {
		name: 'Klient karczmy 2',
		color: '#d4d4d4',
	},
	k3: {
		name: 'Klient karczmy 3',
		color: '#d4d4d4',
	},
	k4: {
		name: 'Klient karczmy 4',
		color: '#d4d4d4',
	},
	w: {
		name: 'Czarnoksiężnik',
		color: '#63d6e6',
	}
});

function playVoice (actor, index, pad) {
	var pad = pad ? pad : 3;

	return `play voice ${actor}${String(index).padStart(pad, '0')}.mp3`;
	// return function () {return true;};
}

function playVoiceWithoutPad (actor, index, pad) {
	var pad = pad ? pad : 3;

	return `play voice ${actor}${String(index)}.mp3`;
	// return function () {return true;};
}

function reversibleTwoWay (func) {
	return {'Function':{
		'Apply': function () {
			return func();
		},
		'Reverse': function () {
			return func();
		}
	}};
}

function ruriri (index, text, unknown) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('ruriri', index), false);
			}
		});
		if (unknown) {
			monogatari.run(`mu ${text}`, false);
		} else {
			monogatari.run(`m ${text}`, false);
		}
		return false;
	});
}

function dubbing (filename, who, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (filename) {
				return monogatari.run(playVoiceWithoutPad(filename, ''), false);
			}
		});
		monogatari.run(`${who} ${text}`, false);
		return false;
	});
}

function szur (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('szur_', index), false);
			}
		});
		monogatari.run(`s ${text}`, false);
		return false;
	});
}

function bakerLady (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('bakerLady_', index), false);
			}
		});
		monogatari.run(`b ${text}`, false);
		return false;
	});
}

function archerLady (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('archerLady_', index, 2), false);
			}
		});
		monogatari.run(`a ${text}`, false);
		return false;
	});
}

function guard (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('guard_', index), false);
			}
		});
		monogatari.run(`g ${text}`, false);
		return false;
	});
}

function sanitize(str) {
	return str.replace(/ /g, "_");
}

function simpleChoice(choices, id) {
	var choice = [];
	for (var i = 0; i < choices.length; i++) {
		choice[choices[i]] = {
			'Text': choices[i],
			'Do': 'jump ' + sanitize(choices[i]) + id
		};
	}
	return {'Choice': choice};
}

function choice3() {
	return {
		'Choice': {
			'Strzelnica': {
				'Text': 'Strzelnica',
				'Do': {'Conditional': {
						'Condition': function () {
							return this.storage('Piekarnia3visible') || this.storage('Karczma3visible');
						},
						'True': 'jump Strzelnica3',
						'False': 'jump Strzelnica4'
					}},
			},
			'Piekarnia': {
				'Text': 'Piekarnia',
				'Condition': function () {
					return !this.storage('Karczma3visible');
				},
				'Do': {'Conditional': {
						'Condition': function () {
							return this.storage('Piekarnia3visible');
						},
						'True': 'jump Piekarnia3',
						'False': 'jump Piekarnia4'
					}},
			},
			'Wieża magiczna': {
				'Text': 'Wieża magiczna',
				'Do': 'jump Wieża_magiczna3',
				'Condition': function () {
					return this.storage('Wieża_magiczna3visible');
				}
			},
			'Karczma': {
				'Text': 'Karczma',
				'Do': {'Conditional': {
						'Condition': function () {
							return this.storage('Karczma3visible');
						},
						'True': 'jump Karczma3',
						'False': 'jump Karczma4'
					}},
			},
		}
	};
}

function visible(key) {
	return {
		'Function': {
			'Apply': function () {
				this.storage()[key + 'visible'] = false;
				return true;
			},

			'Reverse': function () {
				this.storage()[key + 'visible'] = true;
			}
		}
	};
}

function setFalse(key) {
	return {
		'Function': {
			'Apply': function () {
				this.storage()[key] = false;
				return true;
			},

			'Reverse': function () {
				this.storage()[key] = true;
			}
		}
	};
}

function setTrue(key) {
	return {
		'Function': {
			'Apply': function () {
				this.storage()[key] = true;
				return true;
			},

			'Reverse': function () {
				this.storage()[key] = false;
			}
		}
	};
}

function resetStorageStart2() {
	this.storage().Karczma3visible = true;
	this.storage().Strzelnica3visible = true;
	this.storage().Piekarnia3visible = true;
	this.storage().Wieża_magiczna3visible = true;
	this.storage().Cela5visible = true;
	this.storage().nonDilutedPotion = true;
	this.storage().dilutedPotion = false;
	this.storage().money = false;
	this.storage().amulet = false;
}

function resetStorageStrzelnica4() {
	this.storage().Karczma3visible = false;
	this.storage().Strzelnica3visible = false;
	this.storage().Piekarnia3visible = false;
	this.storage().Wieża_magiczna3visible = false;
	this.storage().Cela5visible = true;
	this.storage().nonDilutedPotion = true;
	this.storage().dilutedPotion = false;
	this.storage().money = true;
	this.storage().amulet = false;
}

monogatari.script ({
	Start: [
		'show scene time with fadeIn',
		ruriri(1, '...', true),
		ruriri(2, '...', true),
		ruriri(3, 'Obudź się!', true),
		ruriri(4, 'Nie mamy ani chwili do stracenia. Ja... posłuchaj mnie. Posłuchaj bardzo uważnie tego, co ci teraz powiem.', true),
		ruriri(5, 'Jeśli czegoś nie zrobisz, stanie się tragedia. Za wszelką cenę musisz mnie powstrzymać. Za żadne skarby nie mogę przerwać rytuału.', true),
		ruriri(6, 'Teraz wszyscy... przeze mnie...', true),
		ruriri(7, 'Weź się w garść, no dalej! Zostało już niewiele czasu!', true),
		ruriri(8, 'Nieważne, co się będzie działo, musisz mnie odszukać i powstrzymać. Nie mogę przeszkodzić Czarnoksiężnikowi. Jeśli to zrobię... ziemia...', true),
		ruriri(9, 'Uratuj nas. Błagam. Nie dopuść do tego, co się tu zaraz stanie...', true),
		'show scene black with fadeIn',
		'wait 1000',
		'jump Start2',
	],
	Start2: [
		'show scene well_day with fadeIn',
		'play music lute1 with loop fade 5',
		'wu <em>Dodano eliksir do ekwipunku.</em>',

		'show character g guardAngry at rightish',
		guard(1, 'Szybciej, do cholery. Wleczesz się wolniej od kundla z kulawą nogą, a skomlesz tak samo żałośnie.'),
		'show character g guardIrritated at rightish',
		guard(2, 'Psia krew, jak zwykle dostaję najgorszą robotę.'),
		guard(3, 'I to w dzień, kiedy miałem zabrać Pcheyankę na wzgórze, żeby mogła obejrzeć Czarnoksiężnika.'),
		guard(4, 'Niech pies trąca tego skretyniałego komendanta, jak gdyby służenie u boku elfów nie było już wystarczającym poniżeniem.'),
		'show character g guardAngry at rightish',
		'show character s szurHappy at leftish',
		szur(1, '...wiem, że chcecie mnie uciszyć, ale nie uda wam się to!'),
		'show character s szurTriumphant at leftish',
		szur(2, 'Przejrzałam was i wasze magiczne sztuczki, chcecie sprowadzić na nas wszystkich zagładę! Na szczęście mam coś, co może nas jeszcze uratować...'),
		'show character g guardVeryAngry at rightish',
		guard(5, 'Jeśli stać cię na ciągłe mielenie językiem, powinnaś być też w stanie szybciej chodzić!'),
		guard(6, 'No już, nie mam ochoty patrzeć na twoją plugawą mordę przez cały dzień!'),
		'show character g guardIrritated at rightish',
		'hide character s with fadeOut',
		guard(7, 'A do tego ta cholerna ziemia znowu się trzęsie! Marzę o chwili, kiedy będę mógł wreszcie rzucić to wszystko i pójść do karczmy na soczystego prosiaka...'),
		'hide character g',

		'Gdzie teraz?',
		simpleChoice(['Karczma', 'Strzelnica', 'Piekarnia'], '1'),
	],
	Karczma1: [
		'show scene tavern with fadeIn',
		'<em>Jeśli drzwi nie są otwarte, to znaczy, że jeszcze śpimy. Zapraszamy później.</em>',
		'show scene well_day with fadeIn',
		simpleChoice(['Strzelnica', 'Piekarnia'], '1'),
	],
	Strzelnica1: [
		'show scene archery_training with fadeIn',
		'<em>Trwają ćwiczenia. Nie przeszkadzać.</em>',
		'show scene well_day with fadeIn',
		simpleChoice(['Karczma', 'Piekarnia'], '1'),
	],
	Piekarnia1: [
		'show scene AntiqueBakery1 with fadeIn',
		'play sound shop_bell',
		'show character b piekarzowaSmile at center',
		bakerLady(1, 'Serdecznie witamy kolejnego przybysza w naszym skromnym miasteczku!'),
		'show character b piekarzowaSurprised at center',
		bakerLady(2, 'Potrzebujesz pomocy, moje dziecko? Wyglądasz, jakby zjawa pokazała ci się w biały dzień.'),
		'Co ma się dziś stać na wzgórzu?',
		'show character b piekarzowaSurprised at center',
		bakerLady(3, 'Niebywałe! Więc nie jesteś tu, aby uczestniczyć w rytuale, naszej dumie i sławie?'),
		bakerLady(4, 'Nasze miasteczko znane jest ze swoich obchodów przesilenia letniego.'),
		bakerLady(5, 'Co roku cała okolica zjeżdża się, aby wspólnie przy ognisku i muzyce podziwiać rytuał zapieczętowania, odprawiany przez Czarnoksiężnika.'),
		'show character b piekarzowaSmile at center',
		bakerLady(6, 'Doprawdy, to niezapomniane widowisko! Jego historia sięga daleko w przeszłość, kiedy na tym terenie osiedlili się pierwsi przyjezdni.'),
		bakerLady(7, 'Nasza okolica często nawiedzana jest przez trzęsienia ziemi.'),
		bakerLady(8, 'Nasilają się zwłaszcza w tym okresie.'),
		bakerLady(9, 'Legenda głosi, że to mroczne siły, próbujące wydostać się na zewnątrz, przed którymi nasi przodkowie chronili się właśnie rytuałem.'),
		bakerLady(10, 'Odprawiał go zawsze naczelny czarnoksiężnik, mający umocnić zaklęciami ziemię, która inaczej rozwarłaby się i wychynęłyby z niej tak przerażające monstra, jakich jeszcze nikt żywy wcześniej nie widział.'),
		bakerLady(11, 'I choć obecnie to już tylko historia, którą straszy się niegrzeczne dzieci, sam rytuał nadal przyciąga wielu zainteresowanych.'),
		bakerLady(12, 'To prawdziwe święto magii, kiedy płomienie rozświetlają niebo, a ludzie mogą zapomnieć na chwilę o codziennych troskach.'),
		'Nie uważasz, że może być w tym chociaż krztyna prawdy?',
		'W tym rytuale.',
		bakerLady(13, 'Moje dziecko, nie miej takiej przerażonej miny! Wszyscy wiedzą, że to tylko ludowe bajanie.'),
		bakerLady(14, 'Inaczej oznaczałoby to, że pod naszymi nogami czai się sam diabeł.'),
		bakerLady(15, 'Każdy, kto próbuje ci wmówić, że jest inaczej, robi to tylko po to, żeby cię przestraszyć.'),
		bakerLady(16, 'Ale skoro nie jesteś tu, aby wziąć udział w rytuale, to co sprowadza cię w nasze progi?'),
		'Szukam kogoś.',
		'Młoda dziewczyna, jeszcze nastolatka. Wysoki, uroczy głos. Ma wziąć udział w organizacji dzisiejszego rytuału.',
		'Czy znajdę tu kogoś, kto pasuje do tego opisu?',
		'show character b piekarzowaSurprised at center',
		bakerLady(17, 'Hmmm, pozwól mi pomyśleć.'),
		bakerLady(18, 'Tajemnice rytuału zna tylko Czarnoksiężnik, jednak w przeprowadzaniu go pomagają mu nasi łucznicy.'),
		'show character b piekarzowaSmile at center',
		bakerLady(19, 'Może osobą, której szukasz, jest Y\'udeh – powinna teraz uczestniczyć w próbie generalnej na strzelnicy.'),
		bakerLady(20, 'Jest trochę nieprzewidywalna, ale ma dobrą duszę, jeśli powiesz jej, że potrzebujesz pomocy, powinna z tobą porozmawiać.'),
		bakerLady(21, 'Poznasz ją po długim warkoczu i tym, że nie sposób jej usiedzieć w miejscu.'),
		bakerLady(22, 'I mimo że jest również jedyną przedstawicielką płci pięknej w całej drużynie, niejeden młody żołnierz zazdrości jej sprawności i gibkości.'),
		'Istnieje jakiś sposób, aby zobaczyć się teraz z Czarnoksiężnikiem?',
		'show character b piekarzowaWorried at center',
		bakerLady(23, 'Niestety, Czarnoksiężnik udał się w odosobnione miejsce, aby przygotować się do rytuału. Obawiam się, że nie można się z nim teraz skontaktować.'),
		'Mam eliksir, którego zawartość chcę poznać.',
		bakerLady(24, 'Może Y\'udeh i w tej kwestii będzie w stanie ci pomóc. Wydaje mi się, że jej babka była znachorką. Może wnuczka zna się co nie co na rzeczy.'),
		bakerLady(25, 'Jednak na twoim miejscu byłabym ostrożna, eliksiry rządzą się potężną magią.'),
		bakerLady(26, 'Łatwo zrobić sobie nimi krzywdę.'),
		'Dziękuję za pomoc.',
		'show character b piekarzowaSmile at center',
		bakerLady(27, 'Jeśli będziesz w potrzebie, nie wahaj się tu wrócić. Zawsze chętnie służę pomocą tym, którzy jej szukają.'),
		bakerLady(28, 'A tymczasem weź na drogę tych kilka miedziaków i kawałek chleba, wyglądasz, jakby twoje oczy od wielu dni nie widziały prawdziwego posiłku.'),
		'hide character b with fadeOutr',
		'wu <em>Dodano pieniądze do ekwipunku.</em>',
		setTrue('money'),

		'show scene well_day with fadeIn',
		'Gdzie teraz?',
		simpleChoice(['Karczma', 'Strzelnica', 'Piekarnia', 'Wieża magiczna'], '2'),
	],
	'Piekarnia2': [
		'wu W piekarni pojawiła się właśnie świeża porcja rogalików z masłem. Kolejka ciągnie się przez cały rynek, aż do mostu. Nie masz chyba aż tyle czasu.',
		simpleChoice(['Karczma', 'Strzelnica', 'Wieża magiczna'], '2'),
	],
	'Wieża_magiczna2': [
		'show scene arcade_simple_day with fadeIn',
		'wu Wieża otoczona jest grubym murem. W murze znajdują się drzwi, jednak nie masz do nich klucza.',
		simpleChoice(['Kopnij drzwi', 'Wróć na rynek'], ''),
	],

	'Kopnij_drzwi': [
		'Nic się nie stało, oprócz tego, że teraz boli cię noga.',
		'wu Czego innego można było się spodziewać?',
		'show scene well_day with fadeIn',
		'Gdzie teraz?',
		simpleChoice(['Karczma', 'Strzelnica', 'Piekarnia'], '2'),
	],
	'Wróć_na_rynek': [
		'show scene well_day with fadeIn',
		'Gdzie teraz?',
		simpleChoice(['Karczma', 'Strzelnica', 'Piekarnia', 'Wieża magiczna'], '2'),
	],

	'Karczma2': [
		'wu Ten sam napis, co wcześniej, jednak ktoś domalował pod nim niecenzuralny obrazek.',
		'wu Doprawdy, słaby zmysł estetyczny.',
		'Gdzie teraz?',
		simpleChoice(['Strzelnica', 'Piekarnia', 'Wieża magiczna'], '2'),
	],
	'Strzelnica2': [
		'show scene archery_training with fadeIn',
		'show character a luczniczkaSuspicious at center',
		archerLady(1, 'Nie znam cię! Kim jesteś i czego chcesz?'),
		'...',
		archerLady(2, 'Co to za mina? Nie jestem tym, kogo szukasz?'),
		'Twój głos... masz inny głos, niż można było się spodziewać.',
		archerLady(3, 'Spodziewać? Ktoś coś ci o mnie mówił?'),
		'Piekarzowa powiedziała mi, że znasz się trochę na magicznych miksturach.',
		'Ktoś w nocy podrzucił mi tajemniczy eliksir.',
		'Razem z dziwną wiadomością.',
		'Szukam kogoś, kto pomoże mi odkryć jej znaczenie, niestety Czarnoksiężnik...',
		'show character a luczniczkaExcited at center',
		archerLady(4, 'Ha! Zaczyna się robić ciekawie!'),
		archerLady(5, 'Nie musisz kończyć, pomogę ci!'),
		archerLady(6, 'Przygodzie nigdy nie odmówię!'),
		archerLady(7, 'Chwilowo trwają jeszcze ćwiczenia, ale wróć tu w porze obiadowej, będę na ciebie czekać!'),
		'hide character a with fadeOut',
		'show scene well_day with fadeIn',
		choice3()
	],
	Strzelnica3: [
		'show scene archery_training with fadeIn',
		'<em>Trwają ćwiczenia. Nie przeszkadzać.</em>',
		visible('Strzelnica3'),
		'show scene well_evening with fadeIn',
		choice3()
	],
	Piekarnia3: [
		'show scene AntiqueBakery1 with fadeIn',
		'play sound shop_bell',
		'show character b piekarzowaSmile at center',
		bakerLady(29, 'Czy udało ci się już zadomowić w naszym miasteczku?'),
		'Można tak powiedzieć.',
		'Chcę dowiedzieć się więcej na temat Riruth.',
		'Podobno dobrze ją znasz.',
		'show character b piekarzowaWorried at center',
		bakerLady(30, 'Nie wiem, czy tak bym to nazwała. Kiedyś często przychodziła tu bawić się z moim synem, ale od czasu wypadku już praktycznie jej nie widuję.'),
		'Podobno zaszyła się w wieży Czarnoksiężnika i prawie z niej nie wychodzi.',
		bakerLady(31, 'To, co przeżyła, było prawdziwym koszmarem.'),
		'show character b piekarzowaSurprised at center',
		bakerLady(32, 'Widzisz, Riruth nie zawsze była kształcona na następczynię Czarnoksiężnika.'),
		bakerLady(33, 'Owszem, uczyła się, jak zostać magiczką, ale był w wiosce ktoś, komu nie dorównywała naturalnymi predyspozycjami.'),
		bakerLady(34, 'Tą osobą była jej siostra, Bezhethoh.'),
		bakerLady(35, 'Powiedzieć, że Riruth żyła w jej cieniu, to jak nie powiedzieć nic.'),
		'show character b piekarzowaSmile at center',
		bakerLady(36, 'Wielu chwaliło ją za nadzwyczajną dobroć i odwagę, a jej uroda sprawiała, że adoratorzy niemal ustawiali się w kolejki, by tylko zaszczyciła ich spojrzeniem.'),
		bakerLady(37, 'Do tego była prawdziwym oczkiem w głowie Czarnoksiężnika. „Wykazuje talent do magii, który zdarza się raz na kilkanaście pokoleń!” powiadał.'),
		bakerLady(38, 'Wszyscy spodziewali się, że Bezhethoh zajmie jego miejsce podczas odprawiania rytuału.'),
		'show character b piekarzowaSurprised at center',
		bakerLady(39, 'Jednak kilka dni przed przesileniem wydarzyło się coś, co położyło kres wszystkim jego planom.'),
		bakerLady(40, 'Przeprowadzenie rytuału wymaga kilku konkretnych składników. Najbardziej znanym, bo najtrudniej dostępnym, jest włos dziwowicy z Terrabunkowej przełęczy.'),
		bakerLady(41, 'Ostatnim testem dla umiejętności Bezhethoh przed przeprowadzeniem rytuału miało być dostarczenie go bezpiecznie do miasta.'),
		bakerLady(42, 'Wszyscy myśleli, że to tylko formalność, dziwowice, choć dla większości okrutne, znane są ze swojego przyjaznego usposobienia wobec młodych dziewcząt.'),
		bakerLady(43, 'Ludzie powiadają, że są ucieleśnieniem pragnień i uczuć Panien Młodych, którym w dzień zaślubin obcina się włosy na znak przyjęcia do grona osób zamężnych.'),
		bakerLady(44, 'Legenda głosi, że dziwowice rodzą się właśnie z owych włosów.'),
		bakerLady(45, 'Siostry wyruszyły więc na wyprawę, Riruth z przodu, Bezhethoh ubezpieczając tyły.'),
		'show character b piekarzowaWorried at center',
		bakerLady(46, 'Mijały dni, jednak dziewczęta nie wracały. Kiedy w końcu Riruth ukazała się w bramie miasta z włosem w dłoni, była blada, poplamiona krwią i... sama.'),
		bakerLady(47, 'Nikt nie wie do końca, co się stało, bo Riruth nigdy nie zająknęła się na ten temat słowem.'),
		bakerLady(48, 'Niedługo potem zaszyła się w wieży i od tamtej pory praktycznie z niej nie wychodzi.'),
		'To naprawdę tragiczna historia.',
		'Czy to możliwe, że od tak dawna jedyną osobą, z którą się kontaktuje, jest wyłącznie Czarnoksiężnik?',
		'show character b piekarzowaSmile at center',
		bakerLady(49, 'Kiedy była mała, często widywałam ją rozrabiającą razem z Y\'udeh. Są zresztą w podobnym wieku. Może mimo tych smutnych wydarzeń pozostały przyjaciółkami.'),
		bakerLady(50, 'Udało ci się dowiedzieć od niej czegoś o twoim eliksirze?'),
		'Zmierzam właśnie na spotkanie z nią. Rzeczywiście jest dość nieprzewidywalna, tak jak mówiłaś.',
		bakerLady(51, 'Życzę ci zatem powodzenia, moje dziecko! Uważaj, aby nie stracić równowagi, zawsze im bliżej przesilenia, tym trzęsienia ziemi są mocniejsze.'),
		'Dziękuję.',
		'hide character b with fadeOut',
		visible('Piekarnia3'),
		'show scene well_evening with fadeIn',
		choice3()
	],
	Wieża_magiczna3: [
		'show scene arcade_simple_evening with fadeIn',
		'wu Wieża otoczona jest grubym murem. W murze znajdują się drzwi, jednak nie masz do nich klucza.',
		visible('Wieża_magiczna3'),
		'show scene well_evening with fadeIn',
		choice3()
	],
	Karczma3: [
		'stop music with fade 1',
		'show scene tavern with fadeIn',
		'play music house with fade 1',
		'play sound tavern with loop',
		dubbing('karczma1', 'k1', 'Bis! Bis!'),
		dubbing('karczma2', 'k2', 'Polejcie panu strażnikowi piwa! Niech stracę, na mój koszt!'),
		dubbing('karczma3', 'k3', 'Co za wspaniały występ! Ta trzęsąca się ziemia tylko dodawała całości dramatyzmu!'),
		dubbing('karczma4', 'k4', 'Widać, że ręka nie domaga, nie gra już tak, jak dawniej...'),
		'stop music with fade 1',
		'jump Karczma3.1',
	],
	'Karczma3.1': [
		'stop music with fade 1',
		'show character g guardAngry at center',
		guard(8, 'I na co się tak gapisz?'),
		guard(9, 'Wydawało ci się, że szczytem moich marzeń jest bycie parszywym strażnikiem?'),
		'Jesteś bardem?',
		guard(56, '...'),
		'play music lute2 with loop with fade 5',
		'show character g guardIrritated at center',
		guard(10, '<em>Byłem</em> bardem.'),
		guard(11, 'Początkującym. A potem mnie i mojego brata napadli bandyci i wszystko diabli wzięli. Odrąbali mi środkowe palce u obu rąk, kiedy próbowałem ratować mu tyłek.'),
		guard(12, 'Ale że nie mamy w miasteczku trubadura z prawdziwego zdarzenia, czasem gram im do piwa, kilka miedziaków zawsze wpadnie.'),
		simpleChoice(['Liczą się chęci.', 'Profesjonalistą to on nie jest.'], ''),
		],
		'Profesjonalistą_to_on_nie_jest.': [
			'Słychać, że do ideału jeszcze sporo brakuje.',
			'show character g guardVeryAngry at center',
			guard(13, 'A ty, czego tu szukasz? Od rana węszysz po wszystkich kątach, uważaj, żeby jakiś usłużny mieszkaniec nie posądził cię o szpiegostwo.'),
			'Mam sprawę do Czarnoksiężnika.',
			guard(14, 'To masz pecha, Czarnoksiężnik nie jest dziś dostępny. Możesz spakować manatki i wrócić do siebie.'),
			'...',
		'show character g guardAngry at center',
			guard(15, 'Jeśli nie masz nic więcej do powiedzenia, nie trać mojego czasu i zejdź mi z oczu.'),
			simpleChoice(['Znajdź jakąś fałszywą wymówkę.'], ''),
		],
		'Znajdź_jakąś_fałszywą_wymówkę.': [
			'Znam tego dziwaka, którego prowadziłeś dziś rano do więzienia.',
			'Tak, tak. Od dawna... od dawna ma na pieńku z magiczną społecznością.',
			'Widzisz, gildia magów sąsiedniego miasta poleciła mi przekazać waszemu Czarnoksiężnikowi wszystkie szczegóły, dlatego właśnie koniecznie muszę się z nim dzisiaj widzieć.',
			'g ...',
			'show character g guardIrritated at center',
			guard(17, 'Na jakiej podstawie miałbym ci uwierzyć? Jeśli mam cię zaprowadzić do kogoś, kogo zna, potrzebuję dobrych dowodów.'),
			guard(18, 'Albo solidnej zachęty.'),
			simpleChoice(['Daj strażnikowi pieniądze otrzymane od piekarzowej.'], ''),
		],
		'Daj_strażnikowi_pieniądze_otrzymane_od_piekarzowej.': [
			'wu <em>Stracono pieniądze.</em>',
			setFalse('money'),

			'show character g guardNeutral at center',
			guard(19, 'Marna suma. Ale wyglądasz, jakby to było wszystko, co masz, więc niech będzie.'),
			guard(20, 'Udaj się pod mur otaczający wieżę, kiedy zajdzie słońce, drzwi będą otwarte.'),
			guard(21, 'W wieży powinna być uczennica Czarnoksiężnika, Riruth. Jeśli zapukasz, może wpuści cię do środka.'),
			guard(22, 'Ale radziłbym na nią uważać, w przeszłości straciła w wypadku całą rodzinę i od tego czasu jest dość niestabilna.'),
			'Niestabilna? Wiesz coś więcej na temat tego wypadku?',
				'show character g guardIrritated at center',
			guard(23, 'Nie interesuje mnie specjalnie los rozwydrzonych sierot.'),
			guard(24, 'Piekarzowa pewnie wie o niej coś więcej, w przeszłości bahora często można było spotkać w piekarni, wrzeszczała na cały głos, kiedy zabrało się jej zabawkę.'),
			guard(25, 'Odkąd jej rodzina została zjedzona przez dziwowice, praktycznie nie wychodzi z czarodziejskiej wieży, pewnie całkiem jej odbiło.'),
			'show character g guardAngry at center',
			guard(46, 'Podobnie jak tej plugawej łachmaniarce, którą prowadziłem dzisiaj do więzienia.'),
			'show character g guardVeryAngry at center',
			guard(47, 'Uważa się za oświeconą, ale to zwykła bandytka.'),
			guard(48, 'Nakryto ją, kiedy próbowała napaść na wóz z magicznymi przedmiotami.'),
			guard(49, 'Krzyczała jakieś brednie o ogólnoświatowym spisku magów, który ma wkrótce skończyć się zbiorową masakrą.'),
			'show character g guardIrritated at center',
			guard(50, 'Chociaż czasem trudno jej się dziwić. Ci przeklęci magowie zrobią wszystko, żeby pozbyć się czegokolwiek, co stoi im na drodze.'),
			guard(51, 'Nawet jeśli oznacza to poświęcenie kilku niewinnych cywilów. „Konieczna ofiara”, jak to lubią mówić.'),
			'show character g guardAngry at center',
			guard(52, 'Krew mnie zalewa na samą myśl o tych ich oślizgłych gębach uładzonych czarami.'),
			'show character g guardNeutral at center',
			guard(53, 'Ale dość już o tym. Od tego całego gadania zaschło mi w gardle, a dodatkowo zapachy z kuchni sugerują, że mój przepyszny prosiaczek zaraz wjedzie na stół.'),
			'show character g guardIrritated at center',
			guard(54, 'Zachód słońca, furtka w murze. A teraz zejdź mi z oczu.'),
			'hide character g with fadeOut',
			visible('Karczma3'),
			'stop sound',
			'show scene well_evening with fadeIn',
			choice3(),
		],

	'Liczą_się_chęci.': [
		'Dobrze ci idzie. Brzmisz jak prawdziwy zawodowiec.',
		'show character g guardNeutral at center',
		guard(35, 'Od razu widać, że nie znasz się zupełnie na muzyce. Ale dziękuję za komplement.'),
		guard(36, 'Co cię sprowadza do naszego miasta?'),
		'Mam sprawę do Czarnoksiężnika.',
		guard(37, 'Czarnoksiężnik jest dziś nieuchwytny, przygotowuje się do rytuału.'),
		'Istnieje jakiś sposób, aby spotkać się z nim jeszcze przed świętowaniem?',
		'show character g guardIrritated at center',
		guard(38, 'W normalnej sytuacji powiedziałbym, że nie. Ale zaskoczyłeś mnie swoim dobrym gustem.'),
		guard(39, 'Wiem, z kim możesz porozmawiać. Nie będzie to co prawda sam Czarnoksiężnik, ale osoba z jego najbliższego otoczenia.'),
		'To w zupełności mnie satysfakcjonuje.',
		'show character g guardNeutral at center',
		guard(40, 'Udaj się pod mur otaczający wieżę, kiedy zajdzie słońce, drzwi będą otwarte.'),
		guard(41, 'W wieży powinna być uczennica Czarnoksiężnika, Riruth. Jeśli zapukasz, może wpuści cię do środka.'),
		guard(42, 'Ale radziłbym na nią uważać, w przeszłości straciła w wypadku całą rodzinę i od tego czasu jest dość... niestabilna.'),
		'Niestabilna? Wiesz coś więcej na temat tego wypadku?',
		'show character g guardIrritated at center',
		guard(43, 'Nie interesuje mnie specjalnie los rozwydrzonych sierot.'),
		guard(44, 'Piekarzowa pewnie wie o niej coś więcej, w przeszłości bahora często można było spotkać w piekarni, wrzeszczała na cały głos, kiedy zabrało się jej zabawkę.'),
		guard(45, 'Odkąd jej rodzina została zjedzona przez dziwowice, praktycznie nie wychodzi z czarodziejskiej wieży, pewnie całkiem jej odbiło.'),
		'show character g guardAngry at center',
		guard(46, 'Podobnie jak tej plugawej łachmaniarce, którą prowadziłem dzisiaj do więzienia.'),
		'show character g guardVeryAngry at center',
		guard(47, 'Uważa się za oświeconą, ale to zwykła bandytka.'),
		guard(48, 'Nakryto ją, kiedy próbowała napaść na wóz z magicznymi przedmiotami.'),
		guard(49, 'Krzyczała jakieś brednie o ogólnoświatowym spisku magów, który ma wkrótce skończyć się zbiorową masakrą.'),
		'show character g guardIrritated at center',
		guard(50, 'Chociaż czasem trudno jej się dziwić. Ci przeklęci magowie zrobią wszystko, żeby pozbyć się czegokolwiek, co stoi im na drodze.'),
		guard(51, 'Nawet jeśli oznacza to poświęcenie kilku niewinnych cywilów. „Konieczna ofiara”, jak to lubią mówić.'),
		'show character g guardAngry at center',
		guard(52, 'Krew mnie zalewa na samą myśl o tych ich oślizgłych gębach uładzonych czarami.'),
		'show character g guardNeutral at center',
		guard(53, 'Ale dość już o tym. Od tego całego gadania zaschło mi w gardle, a dodatkowo zapachy z kuchni sugerują, że mój przepyszny prosiaczek zaraz wjedzie na stół.'),
		'show character g guardIrritated at center',
		guard(54, 'Zachód słońca, furtka w murze. A teraz zejdź mi z oczu.'),
		'hide character g with fadeOut',
		'stop sound',
		visible('Karczma3'),
		'show scene well_evening with fadeIn',
		choice3(),
	],

	Piekarnia4: [
		'show scene AntiqueBakery1 with fadeIn',
		'show character b piekarzowaSmile at center',
		bakerLady(52, 'Chyba nie chcesz się spóźnić na spotkanie z Y\'udeh, moje dziecko?'),
		'show character b piekarzowaSmile at center',
		'show scene well_evening with fadeIn',
		choice3()
	],
	Karczma4: [
		'show scene tavern with fadeIn',
		'show character g guardAngry at center',
		guard(55, 'Powiedziałem, zachód słońca, furtka w murze. A teraz zejdź mi z oczu.'),
		'hide character g with fadeOut',
		'show scene well_evening with fadeIn',
		choice3()
	],
	Strzelnica4: [
		'stop music with fade 1',
		'show scene archery_training with fadeIn',
		'show character a luczniczkaSuspicious at center',
		archerLady(8, 'No, wreszcie! Czekałam na ciebie całe wieki!'),
		'Piekarzowa wydaje się bardzo miła.',
		'play music lute3 with loop fade 5',
		'show character a luczniczkaExcited at center',
		archerLady(9, 'Piekarzowa Hik\'ae? Tak, odkąd straciła syna, realizuje swój instynkt macierzyński przez pomoc wszystkim strudzonym i w potrzebie.'),
		archerLady(10, 'A do tego jej maślane rogaliki zawsze potrafią poprawić nastrój!'),
		'Powiedziała, że wiesz, co się stało tamtego dnia z Riruth i jej siostrą.',
		'show character a luczniczkaSuspicious at center',
		archerLady(11, 'Powiedziałam, że ci pomogę, ale najpierw muszę wiedzieć, czego dotyczy twój problem.'),
		archerLady(12, 'To, co chcesz ze mnie wyciągnąć, to bardzo poufne informacje. Muszę wiedzieć, że gra jest warta świeczki!'),
		'W nocy przyśnił mi się przedziwny sen.',
		'Ktoś zostawił mi wiadomość. A raczej... próbował się ze mną skontaktować.',
		'Młoda dziewczyna, bardzo roztrzęsiona.',
		'Mówiła, że za wszelką cenę nie mogę dopuścić, by Czarnoksiężnik został przez nią powstrzymany i że coś złego ma się stać z ziemią.',
		'show character a luczniczkaDepressed at center',
		'A do tego ktoś podrzucił mi w nocy tajemniczy eliksir.',
		'Po obudzeniu okazało się, że jestem u bram waszego miasta.',
		'Albo to naprawdę wyrafinowany żart, albo rzeczywiście może zdarzyć się tu coś naprawdę niepokojącego.',
		'Podejrzewam, że to Riruth mogła mi zostawić tę wiadomość.',
		'Jest magiczką, więc to, że potrafi porozumiewać się z ludźmi w ich myślach, brzmi na prawdopodobne.',
		'Tylko jaki miałaby powód, żeby działać na niekorzyść Czarnoksiężnika, który, wnioskując z opowieści piekarzowej, jest dla niej teraz jedyną namiastką rodziny?',
		archerLady(13, 'Ma bardzo dobry powód.'),
		archerLady(14, 'To, co teraz ci powiem, to ścisła tajemnica.'),
		archerLady(15, 'Piekarzowa pewnie opowiedziała ci, jak Riru razem z siostrą beztrosko wyruszyły na wyprawdę po ostatni składnik potrzebny do odprawienia rytuału.'),
		archerLady(16, 'I jak miała być to jedynie formalność, która potem przerodziła się w niewyjaśnioną tragedię.'),
		'Tak, to właśnie mi mówiła.',
		'show character a luczniczkaSuspicious at center',
		archerLady(17, 'Jednak to nie do końca prawdziwy przebieg tamtych wydarzeń.'),
		archerLady(18, 'W tamtym roku dziwowice były dziwnie rozdrażnione.'),
		archerLady(19, 'Czarnoksiężnik musiał o tym wiedzieć, na wyprawę po włos wysłał bowiem jedynie Riru.'),
		archerLady(20, 'Tak na „wszelki wypadek”, jak to określił, tłumacząc potem swoją decyzję Bezhethoh.'),
		'show character a luczniczkaDepressed at center',
		archerLady(21, 'Bezhethoh ruszyła za siostrą, jak tylko usłyszała, co się wydarzyło, jednak było już za późno.'),
		archerLady(22, 'Sama nie znam szczegółów tego, co się stało na Terrabunkowej przełęczy, ale Riru od tamtej pory wyrzuca sobie, że to ona powinna była zginąć, nie jej siostra.'),
		'To naprawdę smutna historia.',
		'show character a luczniczkaSuspicious at center',
		archerLady(23, 'Możesz spytać, czemu wciąż przebywa tak blisko osoby, która chciała zaprowadzić ją na pewną śmierć, zamiast uciec od Czarnoksiężnika i rozpocząć nowe życie.'),
		archerLady(24, 'Tak, tak, to bardzo słuszne pytanie!'),
		archerLady(25, 'Z tego, co mi wiadomo, poprzysięgła Czarnoksiężnikowi zemstę!'),
		archerLady(26, 'Chce pozbawić go mocy tuż przed samym rytuałem, by nie był w stanie go przeprowadzić, a następnie powiedzieć wszystkim, co tak naprawdę stało się tamtego dnia.'),
		'show character a luczniczkaExcited at center',
		archerLady(27, 'Wyobraź sobie ten dramatyzm! Te emocje!'),
		archerLady(28, 'Już sama myśl o tym sprawia, że staję się podeskcytowana!'),
		archerLady(29, 'W razie czego miałam pomóc jej opanować rozwścieczony tłum!'),
		'show character a luczniczkaSuspicious at center',
		archerLady(30, 'Widzisz, Czarnoksiężnik cieszy się tu dużym szacunkiem.'),
		'show character a luczniczkaExcited at center',
		archerLady(31, 'Dla wielu ludzi to może być prawdziwy cios!'),
		'show character a luczniczkaSuspicious at center',
		archerLady(32, 'Jeśli jednak to, co mówisz, jest prawdą...'),
		'show character a luczniczkaDepressed at center',
		archerLady(33, 'Czy legendy przekazywane z pokolenia na pokolenie miałyby się okazać czymś więcej niż opowieściami?'),
		archerLady(34, 'Czy naprawdę pod ziemią, po której teraz chodzimy, skrywają się demony?!'),
		'Zostało już niewiele czasu. Czy wiesz, jak mogę się z nią skontaktować?',
		'show character a luczniczkaSuspicious at center',
		archerLady(35, 'Znam tajne wejście do wieży, jednak pozostaje jeszcze kwestia muru... Widzisz, jak byliśmy mali, kiedy Czarnoksiężnika nie było, często bawiliśmy się w tamtej okolicy, ja, Riru i dzieci z sąsiedztwa.'),
		'show character a luczniczkaExcited at center',
		archerLady(36, 'Mieliśmy nawet ukryte przejście, wiodące na samą górę wieży.'),
		archerLady(37, 'Dla takich dzieciaków jak my to była niezła frajda, wszystkie te magiczne artefakty, wypchane zwierzęta i oczy wampirów w słoikach.'),
		'show character a luczniczkaSuspicious at center',
		archerLady(38, 'Jednak któregoś dnia wokół wieży stanął gruby mur, którego nie da się przeskoczyć. Klucze mają tylko władze miasta.'),
		'show character a luczniczkaExcited at center',
		archerLady(39, 'Na szczęście Czarnoksiężnik nie był nigdy w stanie odkryć tajnego przejścia.'),
		archerLady(40, 'Riru włożyła dużo pracy w to, żeby ukryć przed nim jego istnienie.'),
		'Mur to nie problem. Strażnik miejski obiecał, że go dla mnie otworzy.',
		'show character a luczniczkaSuspicious at center',
		archerLady(41, 'Który strażnik zgodziłby się na coś takiego?! Zresztą, żaden z nich nie powinien mieć kluczy do wieży.'),
		'Ten, który był kiedyś bardem. Niestety nie znam jego imienia.',
		'show character a luczniczkaOutraged at center',
		archerLady(42, 'Ordennuth?? To najgorszy typek z nich wszystkich!'),
		archerLady(43, 'To on naskarżył na nas Czarnoksiężnikowi tuż przed tym, jak przed wieżą stanął mur!'),
		archerLady(44, 'Pewnie ten, w podzięce za informacje, dał mu klucze. A to łajdak!'),
		'show character a luczniczkaSuspicious at center',
		archerLady(45, 'Kto wie, co knuje, po co inaczej byłby mu potrzebny dostęp do wieży?'),
		archerLady(46, 'Nie wiem, jakim cudem udało ci się go przekonać do pomocy.'),
		'Mam swoje... sposoby.',
		archerLady(47, 'To teraz posłuchaj uważnie. Spod drzwi w murze udaj się pod rosnącą przy wieży olchę. Stamtąd zrób piętnaśnie kroków na zachód i pięć na północ, a następnie obróć wokół własnej osi, najpierw zgodnie ze wskazówkami zegara, potem przeciwnie.'),
		'show character a luczniczkaExcited at center',
		archerLady(48, 'Potem zamknij oczy i policz od tyłu do 10. To powinno załatwić sprawę przejścia.'),
		'Naprawdę byliście mali, kiedy je ukrywaliście.',
		'show character a luczniczkaSuspicious at center',
		archerLady(49, 'Hmpf! Ważne, że działa.'),
		archerLady(50, 'Do rytuału zostało jeszcze trochę czasu. Weź wszystko, czego potrzebujesz, a potem pójdź do niej.'),
		'show character a luczniczkaExcited at center',
		archerLady(51, 'Ja będę namawiać ludzi, aby z powodu silniejszych niż zwykle trzęsień ziemi zostali w domu!'),
		'Pozostaje jeszcze kwestia eliksiru.',
		'show character a luczniczkaSuspicious at center',
		archerLady(52, 'Tak, właśnie! Pokaż mi go. Może będę umiała pomóc.'),
		simpleChoice(['Pokaż elfce eliksir.'], ''),
		],
	'Pokaż_elfce_eliksir.': [
		'show character a luczniczkaExcited at center',
		archerLady(53, 'Ach!'),
		'Coś się stało?',
		archerLady(54, 'To naprawdę potężny eliksir. Osoba, która ci go dała, musiała być bardzo zdesperowana.'),
		archerLady(55, 'Nawet kilka kropel wystarczy, aby pozbawić dorosłego maga całej mocy.'),
		'Pozbawić całej mocy? To trochę przesada.',
		'Nie masz czegoś, czego możemy użyć zamiast?',
		'show character a luczniczkaSuspicious at center',
		archerLady(56, 'Niestety, odkąd moja babka zmarła, nie produkujemy już magicznych mikstur. Jest coś, co mogę zrobić, ale nie wiem, czy to rozwiązanie cię usatysfakcjonuje.'),
		archerLady(57, 'Mogę go rozcieńczyć, pozbawiając go tym samym części mocy. Wtedy jeśli zadziała, jedynie tymczasowo ogłuszy Riru, ale jej moce pozostaną nietknięte.'),
		'Jeśli zadziała?',
		'show character a luczniczkaDepressed at center',
		archerLady(58, 'No właśnie. Niestety ingerując w jego skład, rośnie ryzyko, że utraci część swoich własności. Krótko mówiąc, może się zdarzyć, że przestanie działać.'),
		simpleChoice(['Rozcieńcz eliksir.','Nie rozcieńczaj eliksiru.'], ''),
	],

	'Rozcieńcz_eliksir.': [
		'wu <em>Stracono eliksir. Dodano rozcieńczony eliksir do ekwipunku.</em>',
		setTrue('dilutedPotion'),
		setFalse('nonDilutedPotion'),
		'Wnioskując z opowieści, pozbawienie jej mocy to cios, którego może nie znieść. Nawet, jeśli eliksir nie zadziała, na pewno znajdzie się jakieś inne wyjście, żeby ją powstrzymać.',
		'show character a luczniczkaSuspicious at center',
		archerLady(59, 'To wybór, za który odpowiedzialność bierzesz na siebie. Ja mogę jedynie trzymać kciuki, żeby wszystko skończyło się dobrze.'),
		'show character a luczniczkaExcited at center',
		archerLady(62, 'Nie traćmy więcej czasu. Kiedy będziesz gotów, idź do wieży! Ja pójdę porozmawiać z mieszkańcami.'),
		archerLady(60, 'Kiedy to wszystko się skończy, daj mi znać.'),
		archerLady(61, 'Zarówno płakać, jak i świętować, jest lepiej w większym gronie.'),
		'jump Rynek5',
	],
	'Nie_rozcieńczaj_eliksiru.': [
		'To za duże ryzyko. Z tego, co mówiła mi we śnie, grozi nam prawdziwa tragedia.',
		'Nie możemy jej zlekceważyć.',
		'show character a luczniczkaSuspicious at center',
		archerLady(59, 'To wybór, za który odpowiedzialność bierzesz na siebie. Ja mogę jedynie trzymać kciuki, żeby wszystko skończyło się dobrze.'),
		'show character a luczniczkaExcited at center',
		archerLady(62, 'Nie traćmy więcej czasu. Kiedy będziesz gotów, idź do wieży! Ja pójdę porozmawiać z mieszkańcami.'),
		archerLady(60, 'Kiedy to wszystko się skończy, daj mi znać.'),
		archerLady(61, 'Zarówno płakać, jak i świętować, jest lepiej w większym gronie.'),
		'jump Rynek5',
	],

	Rynek5: [
		'stop music with fade 1',
		'show scene well_evening with fadeIn',
		'play music lute4 with fade 1',
		'play sound party',
		{'Conditional': {
				'Condition': function () {
					return this.storage('Cela5visible');
				},
				'True': simpleChoice(['Strzelnica', 'Karczma', 'Piekarnia', 'Wieża magiczna', 'Cela'], 5),
				'False': simpleChoice(['Strzelnica', 'Karczma', 'Piekarnia', 'Wieża magiczna'], 5)
		}},
	],

	Strzelnica5: [
		'wu <em>Y\'udeh już tu nie ma.</em>',
		'jump Rynek5',
	],

	Karczma5: [
		'wu <em>Wszyscy wybrali się obejrzeć rytuał.</em>',
		'jump Rynek5',
	],

	Piekarnia5: [
		'wu <em>Piekarzowa wybrała się obejrzeć rytuał.</em>',
		'jump Rynek5',
	],

	Cela5: [
		'stop sound with fade 1',
		'stop music with fade 1',
		'show scene prison_cell_day with fadeIn',
		'play music dungeon with loop fade 1',
		'show character s szurHappy at center',
		szur(3, 'Już niedługo cały świat dowie się o waszym spisku... wtedy okaże się, że ja, Duilleach Aloeh, miałam jednak rację, cały ten czas!'),
		'show character s szurAngry at center',
		szur(4, 'Nawet kiedy mnie wyszydzaliście, poniżaliście...'),
		'show character s szurHappy at center',
		szur(5, 'Przypomnicie to sobie, będą was wycinać w pień swoimi diabelskimi zaklęciami... Ja natomiast będę bezpieczna... bezpieczna...'),
		'O jakim spisku mówisz? Co takiego ma się stać?',
		'show character s szurWorried at center',
		szur(6, 'Kim jesteś i co tu robisz? Ciebie też nasłali, żeby mnie dręczyć? Też będziesz mnie wyśmiewać, jak wszyscy inni twoi znajomi? Durnie, nie wiecie, co nadchodzi!'),
		'show character s szurAngry at center',
		szur(7, 'Oszołomili was migającymi światełkami i obietnicami dobrobytu... ale przecież dobrobyt nie przychodzi za darmo.'),
		szur(8, 'Sami kopiecie sobie własny grób! Oni tylko czekają na dogodny moment, żeby się was pozbyć i przejąć władzę nad wszystkimi, którzy jeszcze pozostali przy życiu...'),
		'show character s szurHappy at center',
		szur(9, 'Jedynie ja przejrzałam wasze plany... Wiem, że ma dziś dojść do wielkiej masakry. Nie zwiedziecie mnie! Gdybyście chociaż chwilę pomyśleli samodzielnie, sami doszlibyście do takiego wniosku...'),
		'show character s szurTriumphant at center',
		szur(10, 'Prawie mi was żal, w tym waszym naiwnym zadufaniu. Ufacie wszystkiemu, co wam mówią. Tymczasem jesteście właśnie prowadzeni jak świnie na rzeź!'),
		'show character s szurWorried at center',
		szur(11, 'A oni nawet nie starają się tego ukrywać... Coraz częstsze trzęsienia ziemi nie mogłyby być już bardziej oczywiste. Ale wy ciągle zaprzeczacie, tak bardzo wyprali wam mózgi...'),
		'show character s szurTriumphant at center',
		szur(12, 'Na szczęście przejrzałam was odpowiednio wcześnie... taak, myśleliście, że to o pieniądze mi chodziło... zabraliście mi wszystko, co wartościowe, ale tego nie udało wam się mi odebrać...'),
		'Chcę to kupić.',
		'show character s szurAngry at center',
		szur(13, 'Co? Nadal tu jesteś? Czego jeszcze chcesz, nie wystarczająco podobni do ciebie już mnie wyszydzali i kpili ze mnie?'),
		'Chcę kupić przedmiot, o którym mówisz.',
		'show character s szurTriumphant at center',
		szur(14, 'Aha! Więc teraz, w obliczu zagrożenia, przychodzisz do mnie, skamląc jak szczeniak, z nadzieją, że podzielę się z tobą, podczas gdy tacy jak ty na każdym kroku kopali mnie i wyśmiewali moje prośby o pomoc!'),
		'Miałaś rację przez cały ten czas. Przejrzałaś ich, zanim my to uczyniliśmy. Byliśmy głupi, och, jakże głupi byliśmy! Z pokorą proszę cię o zlitowanie się nade mną, nic nieznaczącym robakiem. Zasługuję jedynie na najgorsze.',
		'show character s szurHappy at center',
		szur(15, 'Teraz to widzisz! Jak to jest czuć się pokonanym? Poniżonym? Hahahahahaha! Macie za swoje, głupcy!'),
		'show character s szurTriumphant at center',
		szur(16, 'Ale nawet, gdyby oferowano mi górę pieniędzy, nie oddam swojego ratunku! Nie postradałam jeszcze zmysłów! Ja i mój amulet tłumiący magię pozdrowimy cię, gdy już trafisz do zaświatów!'),
		'A jeśli mam przy sobie coś, co jest nawet bardziej potężne niż twój amulet?',
		'Czy wtedy mi go sprzedasz?',
		'show character s szurWorried at center',
		szur(17, 'Jeszcze potężniejsze?'),
		'show character s szurAngry at center',
		szur(29, 'Próbujesz mnie zwieść. W momencie, gdy wyciągnę swój amulet, zabierzesz mi go i uciekniesz. Ja już znam takich jak ty. Nie nabierzesz mnie! Nie odbierzesz mi mojego ratunku!'),
		simpleChoice(['Wyciągnij z kieszeni eliksir.'], ''),
	],
	'Wyciągnij_z_kieszeni_eliksir.': [
		'show character s szurWorried at center',
		szur(18, 'Ochh! Ten eliksir...'),
		'show character s szurHappy at center',
		szur(19, 'Słyszałam o nim, to najpotężniejszy eliksir antymagiczny znany w tych stronach...'),
		'show character s szurWorried at center',
		szur(20, 'Ale dlaczego chcesz wymienić się nim za amulet tłumiący magię? Próbujesz mnie wykiwać?'),
		'Mam swoje powody.',
		'show character s szurAngry at center',
		szur(21, 'Coś mi tu śmierdzi! Zdecydowanie to pułapka! Próbujesz mnie oszukać... chociaż eliksir jest prawdziwy, co do tego nie ma wątpliwości... z jego potęgą na pewno będę bezpieczna...'),
		'To oferta ograniczona czasowo. Decydujesz się albo zabieram swój eliksir i wychodzę.',
		'show character s szurTriumphant at center',
		szur(22, 'No dobrze... ale na potwierdzenie swoich słów oprócz eliksiru chcę jeszcze pieniądze. Wtedy zgodzę się na tę wymianę!'),
		'jump Cela5Wybor1',
	],
	Cela5WyborNoMoney: [
		'wu <em>Niestety nie masz pieniędzy.</em>',
		simpleChoice(['Nie zgódź się na wymianę.'], '')
	],
	Cela5Wybor1: [
		simpleChoice(['Wymień eliksir i pieniądze od piekarzowej na amulet wytłumiający magię.', 'Nie zgódź się na wymianę.'], '')
	],
	'Wymień_eliksir_i_pieniądze_od_piekarzowej_na_amulet_wytłumiający_magię.': [
		{'Conditional': {
				'Condition': function () {
					return this.storage().money;
				},
				'True': function () {
					return true;
				},
				'False': 'jump Cela5WyborNoMoney'
		}},
		'wu <em>Stracono eliksir i pieniądze. Dodano do ekwipunku amulet wytłumiający magię.</em>',
		setFalse('money'),
		setTrue('amulet'),
		'show character s szurHappyBezAmulet at center',
		szur(23, 'O taaaak, teraz jestem już na pewno bezpieczna! Teraz już nic mi nie zrobicie...'),
		'show character s szurWorriedBezAmulet at center',
		szur(24, 'A ty zostaw mnie w spokoju, zanim wyleję na ciebie kroplę, kto wie, ile dodatkowych nóg i rąk ci wyrośnie! Hahahahahah!'),
		'hide character s with fadeOut',
		'stop music with fade 1',
		visible('Cela5'),
		'jump Rynek5',
	],
	'Nie_zgódź_się_na_wymianę.': [
		'Nie ufasz mi. No cóż, to twoja strata. W takim razie zbieram się do wyjścia.',
		'show character s szurWorried at center',
		szur(25, 'Poczekaj! Przemyślałam to, przyjmuję twoją ofertę. Żadnych dodatkowych opłat. Amulet za eliksir i jesteśmy kwita.'),
		simpleChoice(['Wymień eliksir na amulet wytłumiający magię.', 'Nie zgódź się na wymianę.'], '2')
	],

	'Wymień_eliksir_na_amulet_wytłumiający_magię.2': [
		'wu <em>Stracono eliksir. Dodano do ekwipunku amulet wytłumiający magię.</em>',
		setTrue('amulet'),
		'show character s szurHappyBezAmulet at center',
		szur(23, 'O taaaak, teraz jestem już na pewno bezpieczna! Teraz już nic mi nie zrobicie...'),
		'show character s szurWorriedBezAmulet at center',
		szur(24, 'A ty zostaw mnie w spokoju, zanim wyleję na ciebie kroplę, kto wie, ile dodatkowych nóg i rąk ci wyrośnie! Hahahahahah!'),
		'hide character s with fadeOut',
		'stop music with fade 1',
		visible('Cela5'),
		'jump Rynek5'
	],
	'Nie_zgódź_się_na_wymianę.2': [
		'Cóż, miałaś swoją szansę. Oferta przestała być aktualna. Masz rację, ten eliksir jest o wiele pewniejszy niż twój amulet. Czuję, że będzie mi dobrze służył.',
		'show character s szurWorried at center',
		szur(28, 'Nie odchodź! Zrobię wszystko, podam ci ich imiona! Jak tylko mnie stąd wypuszczą, będę ci wiernie służyć! Naprawdę, zrobię wszystko!'),
		'Żegnaj.',
		'hide character s with fadeOut',
		visible('Cela5'),
		'stop music with fade 1',
		'jump Rynek5'
	],

	'Wieża_magiczna5': [
		'stop music with fade 1',
		{'Conditional': {
				'Condition': function(){
					if(this.storage().amulet) {
						console.log('Amulet -> Zakończenie2.');
						return 'Zakończenie2';
					} else if (this.storage().dilutedPotion) {
						console.log('DilutedPotion -> Zakończenie1.');
						return 'Zakończenie1';
					} else if (this.storage().nonDilutedPotion){
						console.log('NonDilutedPotion -> Zakończenie3.');
						return 'Zakończenie3';
					} else {
						console.log('No condition satisfied, defaulting to Zakończenie1.');
						return 'Zakończenie1';
					}
				},
				'Zakończenie1': 'jump Zakończenie1',
				'Zakończenie2': 'jump Zakończenie2',
				'Zakończenie3': 'jump Zakończenie3',
		}},
		'show scene potion_shop with fade 1',
		'jump Zakończenie1',
		],
	//Zakończenie 1: Nie wymieniliśmy się z szurem, a eliksir został rozcieńczony
	'Zakończenie1': [
		'show scene potion_shop with fadeIn',
		'play music finalLabor with loop fade 1',
		'<em>W którejś z komnat słychać donośny męski głos, śpiewający balladę o królowej wróżek. Z pewnością nikt poza Czarnoksiężnikiem i Riruth tu nie przychodzi. Poza tym, że trochę zakurzone, to całkiem niezłe miejsce do ćwiczeń. Ma wspaniałe echo.</em>', // kursywą
		'Zagadka, po co był mu potrzebny dostęp do wieży, rozwiązana.',
		'Lepiej będzie, jeśli się pospieszę, sądząc po odgłosach, zostało już niewiele czasu.',
		'show character m magicianSad at center',
		ruriri(63, 'Tylko spokojnie. Oddychaj. Wszystko będzie dobrze.'), // przytłumiony głos, myśli, że jest sama
		ruriri(64, 'Pokażesz im, na co cię stać.'),
		ruriri(65, 'Po tych wszystkich latach wreszcie nadszedł czas, kiedy ludzie poznają prawdę. Zobaczą, z czym musiałam żyć tyle lat. Zobaczą, że nie jestem tylko cieniem siostry.'),
		ruriri(66, 'Jeszcze raz: wypowiem zaklęcie ze strony 278, potem z 352... nie, nie, 351. W pokoju powinien rozbłysnąć różowy płomień, to oznacza, że klątwa zadziałała i stary dziad nie będzie mógł używać czarów aż do ponownego wschodu słońca.'),
		ruriri(67, 'Wtedy przeteleportuję się na wzgórze i opowiem wszystkim, jaki jest naprawdę ich ukochany, dobry Czarnoksiężnik.'),
		ruriri(68, 'A potem... a potem sama przeprowadzę rytuał.'),
		ruriri(69, 'Wszyscy tam będą... ich oczy wpatrzone prosto we mnie... wyczekujące każdego słowa...'),
		'show character m magicianAngry at center',
		ruriri(70, 'Potrafisz, Riru, potrafisz! Przez te wszystkie lata ćwiczyłaś właśnie dlatego momentu!'),
		ruriri(71, 'Poza tym to tylko wystrzelenie kilku płomieni, wypowiedzenie formułki i fajerwerki.'),
		ruriri(72, 'Bułka z masłem.'),
		'Przepraszam, że nachodzę tak znienacka. To pilna sprawa.',
		'show character m magicianScared at center',
		ruriri(73, 'Ach??? Ktoś tu jest?? Jakim cudem udało ci się wejść, i brama, i wieża są szczelnie zamknięte!'),
		'Y\'udeh zdradziła mi tajemne przejście, a strażnik otworzył drzwi. Proszę, posłuchaj co mam do powiedzenia.',
		ruriri(74, 'Znasz Yu? I skąd strażnik miałby mieć klucz, o czym ty mówisz? Kim jesteś i czego ode mnie chcesz?!'),
		'Sama zostawiłaś mi wiadomość. Mówiłaś do mnie z przyszłości. Coś bardzo złego się stanie, jeśli wypowiesz to zaklęcie.',
		'show character m magicianSad at center',
		ruriri(75, 'Coś bardzo złego?'),
		'Mówiłaś, że Czarnoksiężnik nie może zostać pozbawiony mocy, bo inaczej coś złego stanie się z ziemią i wszystkimi uczestnikami rytuału. Mówiąc to, płakałaś. Dlatego jestem tu, by powstrzymać się od wypowiedzenia zaklęcia. Sama mnie o to prosiłaś!',
		'show character m magicianAngry at center',
		ruriri(76, 'Ja... nie... niemożliwe! Jak miałabym do ciebie mówić, nigdy wcześniej cię nie widziałam!'),
		ruriri(77, 'Poza tym wszyscy wiedzą, że rytuał to tylko tradycja i nawet jeśli Czarnoksiężnik nie będzie w stanie go przeprowadzić, to nic się nie stanie.'),
		ruriri(78, 'Nareszcie nadszedł czas, żeby spotkało go zasłużone upokorzenie. I żeby wszyscy się dowiedzieli, jakim naprawdę jest potworem!'),
		ruriri(79, 'To nie może czekać. Już wszystko przygotowałam. To musi stać się dzisiaj podczas rytuału!'),
		'show character m magicianSad at center',
		ruriri(80, 'A jeśli ten rytuał, to rzeczywiście coś więcej?'),
		ruriri(81, 'Jeśli te trzęsienia ziemi nie były przypadkowe?'),
		ruriri(82, 'Jeśli to nie tylko wyczarowanie kilku fajerwerków?'),
		ruriri(83, 'Co, jeśli... jeśli nie będę w stanie sobie poradzić?'),
		'show character m magicianTriumphant at center',
		ruriri(84, 'To nawet lepiej! Wtedy to ja będę tym, kto powstrzyma katastrofę!'),
		ruriri(85, 'Wreszcie im pokażę... pokażę na co mnie stać!'),
		'Nie uda ci się. Już próbowałaś. Musiałaś cofnąć czas, starając się, żeby do tego nie doszło. Dlatego prosiłaś mnie, żeby cię powstrzymać!',
		'show character m magicianAngry at center',
		ruriri(86, 'Ty też we mnie nie wierzysz! Jak oni wszyscy, patrzą się na mnie wciąż z tym politowaniem, udają miłych, żeby potem, kiedy myślą, że nie słyszę, wspominać moją siostrę i żałować, że to ja przeżyłam, a nie ona!'), // wybucha
		'show character m magicianSad at center',
		ruriri(87, 'Myślisz, że to łatwe? Że sama, leżąc w nocy w łóżku, nie zastanawiam się, czy może mają rację i rzeczywiście to ona powinna przeżyć zamiast mnie?'),
		ruriri(88, 'Te myśli nie dają mi spokoju, nie pozwalają spać. Nie potrafię już patrzeć się im w twarz.'),
		ruriri(89, 'Dlatego ta wieża stała się moim domem, więzieniem, z którego nie mogę się wydostać. Jedynym sposobem, aby się zemścić, jest pokonać go jego własną bronią, jego ukochaną magią.'),
		'show character m magicianAngry at center',
		ruriri(90, 'Tyle lat ćwiczyłam, żeby wreszcie móc to zrobić. Żeby wreszcie dosięgnęła go sprawiedliwość.'),
		'show character m magicianTriumphant at center',
		ruriri(91, 'Ooo tak, cóż za słodki to będzie widok, zobaczyć przerażenie na tej jego starej, zgrzybiałej twarzy, zmyć z niej ten paskudny, wszechwiedzący uśmieszek.'),
		'show character m magicianSad at center',
		ruriri(92, 'Twarzy człowieka, który nie zawahał się wysłać na śmierć dziecka w imię swoich własnych interesów...'),
		ruriri(93, 'Pewnie nie chciał, żebym pałętała się mu pod nogami, dlatego wykorzystał dogodną okazję.'),
		'show character m magicianAngry at center',
		ruriri(94, 'A moja tragiczna śmierć miała jeszcze bardziej zmotywować siostrę do zostania najlepszą magiczką, którą widziała ta kraina.'),
		ruriri(95, 'Wiedział, że nie zdąży mnie dogonić, dlatego pozwolił jej iść.'),
		'show character m magicianSad at center',
		ruriri(96, 'Jednak w swoich obliczeniach nie przewidział, że – ha, o ironio – zgubię się i znalezienie szlaku zajmie mi tyle czasu co jej dotarcie do mnie.'),
		ruriri(97, 'Bezhet... nadal mogłabyś żyć, gdyby nie on. Gdyby tylko nie ten przeklęty, obrzydliwy starzec!'),
		'show character m magicianTriumphant at center',
		ruriri(98, 'Dlatego należy mu się kara. Czas zemsty nadchodzi.'),
		ruriri(99, 'Kiedy tylko ludzie z miasta dowiedzą się, co zaszło, na pewno postanowią go wypędzić.'),
		ruriri(100, 'A ja, czarodziejka, której udało się odprawić rytuał, zajmę jego miejsce.'),
		'Wiem, że to trudne, ale na pewno istnieje jakieś inne wyjście. Co jeśli oni wszyscy zginą? Czy wtedy sama nie staniesz się Czarnoksiężnikiem?',
		'show character m magicianSad at center',
		ruriri(101, 'Przestań, nie... nie mów tak! To, co robię, to jedyna słuszna droga. Za zbrodnię należy się kara.'),
		ruriri(102, 'On jest potworem, a ja... ja tylko pilnuję, żeby sprawiedliwości stało się zadość!'),
		'show character m magicianTriumphant at center',
		ruriri(103, 'Poza tym, jeśli cokolwiek się stanie, to ja ich obronię. Potrafię. Jestem czarodziejką zdolną zastąpić Czarnoksiężnika. Tyle ćwiczyłam...'),
		'A jeśli jednak nie dasz sobie rady?',
		'show character m magicianAngry at center',
		ruriri(104, 'Przestań! Mącisz mi w głowie... czy to on cię tu przysłał? Jesteś jego pachołkiem?'),
		ruriri(105, 'To dlatego udało ci się tu wejść! Teraz już wszystko rozumiem...'),
		'show character m magicianSad at center',
		ruriri(106, 'Ale czy w takim razie wie, co ma się tu stać? Nie, to niemożliwe. Jakoś by zareagował! A może tylko podejrzewa? Może obserwuje mnie przez cały ten czas?'),
		ruriri(107, 'Nie, nie, oddal od siebie te myśli, one tylko sprawiają, że stajesz się słabsza! Nieważne, czy wie, czy nie. Trudno. Kości zostały rzucone.'),
		'play sound party with loop',
		'show character m magicianScared at center',
		ruriri(108, 'Rytuał już zaraz się zacznie! Muszę się pospieszyć. To moja jedyna szansa.'),
		'show character m magicianSad at center',
		ruriri(109, 'Jeśli nic nie zrobię, znowu będę musiała czekać na kolejną szansę cały rok. Ile razy byłam już u progu i w końcu stchórzyłam...'),
		'show character m magicianTriumphant at center',
		'stop music with fade 1',
		ruriri(110, 'Jednak tym razem będzie inaczej. To nie czas na wątpliwości. Nie ma ani chwili do stracenia.'),
		// czarodziejka podświetlona na magicznie, będzie wypowiadać zaklęcie
		'show character m magicianAngry at center',
		ruriri(111, 'Popamiętasz, głupcze! Zobaczysz, na co mnie stać, biedną, słabą czarodziejkę, na którą przez tyle lat patrzyłeś z politowaniem!'),
		ruriri(112, 'Czas już podnieść kurtynę i powiedzieć wszystkim prawdę o twoich występkach!'),
		'show character m magicianSadGlow at center',
		'play music elegy with loop',
		ruriri(113, 'Nonhanipa uenigra, nonpono, saimor, acore, tettegrau. Utduli Alo utpe, tebstrirum. Utduli Alo utpe, tebstriru. Scruuosri quamhocbstri, cumlortis, ie teinccle.'),
		simpleChoice(['Użyj eliksiru.'], '2'),
	],
	'Użyj_eliksiru.2': [
		'stop sound',
		'stop music',
		'show character m magicianScared at center',
		ruriri(61, 'Ach! Co się dzieje... wszędzie zrobiło się tak ciemno... czyli jednak to on cię przysłał... jeśli spróbuję skupić wolę, to na pewno uda mi się go...'),
		'hide character m with fadeOut',
	  	'play sound bodyfall',
		ruriri(62, '...'),

		'wait 1000',
		'Zasnęła.',
		'Czyli... udało się?',
		'Wszyscy zostali uratowani?',
		'Co z fajerwerkami?',
		'Oklaskami?',
		'Podejrzanie tu spokojnie.',
		'Nie tak wygląda los bohatera, o którym czyta się w książkach.',
		'No ale... nikt oprócz Y\'udeh nie wie, że tu jestem.',
		'...',
		'Jak tu cicho.',
		'Słychać jedynie ludzi bawiących się na zewnątrz.',
		'Czyli koniec końców Y\'udeh nie powstrzymała ich przed pójściem na wzgórze.',
		'Gdyby mi się nie udało, oni wszyscy prawdopodobnie straciliby życie.',
		'Doszłoby do zbiorowej tragedii.',
		'Riruth chyba nic się nie stało? Rzeczywiście eliksir jedynie ją oszołomił. Skoro zadziałał, to znaczy, że z jej mocą powinno być wszystko w porządku.',
		'Ciekawe, kiedy się obudzi.',
		'wait 1000',
		'I co teraz?',
		'Teraz, kiedy moje zadanie już się skończyło, ten świat wydaje się dziwnie obcy.',
		'Nie mam tu już niczego więcej do załatwienia.',
		'Chyba nie ma sensu dłużej tego przeciągać.',

		'play sound footsteps',
		'stop music with fade 1',
		'wait 500',
		'play sound doorOpenSqueek',

		'play music hero',
		'w Gratulacje! Udało ci się dokonać właściwych wyborów.',
		'w Dzięki nim nikomu nic się nie stało.',
		'w Nie doszło do zbiorowej masakry, a czarodziejka nie straciła mocy.',
		'w Na twoim miejscu byłbym z siebie bardzo dumny.',
		'w Nie każdy był w stanie doprowadzić do dobrego zakończenia.',
		'w Niektórzy poddawali się w połowie, zsyłając tym samym na całe nasze miasto całkowitą destrukcję bez możliwości ratunku.',
		'w Inni byli usatysfakcjonowani jedynie połowicznym sukcesem.',
		'w Ale wiedziałem, że w twoim przypadku będzie inaczej.',
		'w Kim jestem? Jeszcze się nie domyślasz?',
		'w Czarnoksiężnikiem, a kimże innym!',
		'w Bacznie obserwowałem wszystkie twoje poczynania.',
		'w Nie powiem, niektóre bardzo mnie zdziwiły.',
		'w Inne, nie obraź się, ale można było łatwo przewidzieć.',
		'w Czasem, kiedy widziałem, że słabo sobie radzisz, podpowiadałem ci, co masz zrobić.',
		'w A czasami to były tylko podpuchy, mające poprawić ci humor!',
		'w Chociaż przez większość czasu starałem się zostawić ci wolną rękę.',
		'w I było warto!',
		'w Nawet z moimi podpowiedziami, to dobre zakończenie to wciąż głównie twoje zwycięstwo.',
		'w Zasługujesz, aby poklepać się po plecach z wyrazem triumfu.',
		'w Zwłaszcza, że nikt inny nie zrobi tego za ciebie.',
		'w Nie czekają na ciebie żadne fanfary ani podziękowania.',
		'w Co więcej, ludzie prawdopodobnie nigdy nie zdadzą sobie sprawy, że ich życie właśnie zostało przez ciebie uratowane.',
		'w Ale tak trzeba było postąpić.',
		'w Co z Y\'udeh, pytasz? Ona przecież wie?',
		'w Za tydzień nie będzie już o niczym pamiętać.',
		'w Jest przekonana, że to wszystko to jedynie interesujący żart.',
		'w A Riruth? Riruth całą swoją uwagę skupi na kolejnej próbie.',
		'w Tym razem nie była gotowa. Jeszcze nie.',
		'w Ale wierzę, że w końcu jej się uda.',
		'w A ja będę mógł udać się na zasłużoną emeryturę.',
		'w Do tego czasu, jakby to ująć, ubezpieczam tyły.',
		'w Przyznam, że nawet nabrałem w tym już wprawy. Straciłem rachubę, ile razy musiałem ratować sytuację.',
		'w Ale mam przynajmniej pewność, że kiedy Riruth już się uda, będzie najsilniejszą magiczką, jaka kiedykolwiek chodziła po tej ziemi.',
		'w Nawet ja nie mam siły potrzebnej do powstrzymania demona.',
		'w Ale pora już na mnie, rytuał sam się nie odprawi!',
		'w Tobie też należy się zasłużony odpoczynek.',
		'w Dołącz do mnie! Rytuał jest naprawdę wspaniały, szkoda byłoby go przegapić!',
		'w Za cały ten włożony trud zdecydowanie należy ci się jakaś nagroda.',
		'w Będą tam wszyscy, których udało ci się uratować!',
		'w I choć nie zdają sobie sprawy, że to właśnie tobie zawdzięczają życie, czy nie miło będzie pobawić się razem z nimi?',
		'w I kto wie?',
		'w Może jeszcze kiedyś zobaczysz się z nimi ponownie?',
		'show scene festival with fadeIn',
		'w A tymczasem niech zabawa trwa aż do samego rana!',

		'stop music with fade 5',
		'end',
	],
	'Zakończenie2': [
		'stop music with fadeOut',
		'show scene potion_shop with fadeIn',
		//Zakończenie 2: Wymieniliśmy się z szurem na Amulet
		'play music finalLabor with loop fade 1',
		'<em>W którejś z komnat słychać donośny męski głos, śpiewający balladę o królowej wróżek. Z pewnością nikt poza Czarnoksiężnikiem i Riruth tu nie przychodzi. Poza tym, że trochę zakurzone, to całkiem niezłe miejsce do ćwiczeń. Ma wspaniałe echo.</em>',
		'Zagadka, po co był mu potrzebny dostęp do wieży, rozwiązana.',
		'Lepiej będzie, jeśli się pospieszę, sądząc po odgłosach, zostało już niewiele czasu.',
		'show character m magicianSad at center',
		ruriri(63, 'Tylko spokojnie. Oddychaj. Wszystko będzie dobrze.'),
		ruriri(64, 'Pokażesz im, na co cię stać.'),
		ruriri(65, 'Po tych wszystkich latach wreszcie nadszedł czas, kiedy ludzie poznają prawdę. Zobaczą, z czym musiałam żyć tyle lat. Zobaczą, że nie jestem tylko cieniem siostry.'),
		ruriri(66, 'Jeszcze raz: wypowiem zaklęcie ze strony 278, potem z 352... nie, nie, 351. W pokoju powinien rozbłysnąć różowy płomień, to oznacza, że klątwa zadziałała i stary dziad nie będzie mógł używać czarów aż do ponownego wschodu słońca.'),
		ruriri(67, 'Wtedy przeteleportuję się na wzgórze i opowiem wszystkim, jaki jest naprawdę ich ukochany, dobry Czarnoksiężnik.'),
		ruriri(68, 'A potem... a potem sama przeprowadzę rytuał.'),
		ruriri(69, 'Wszyscy tam będą... ich oczy wpatrzone prosto we mnie... wyczekujące każdego słowa...'),
		'show character m magicianAngry at center',
		ruriri(70, 'Potrafisz, Riru, potrafisz! Przez te wszystkie lata ćwiczyłaś właśnie dlatego momentu!'),
		ruriri(71, 'Poza tym to tylko wystrzelenie kilku płomieni, wypowiedzenie formułki i fajerwerki.'),
		ruriri(72, 'Bułka z masłem.'),
		'Przepraszam, że nachodzę tak znienacka. To pilna sprawa.',
		'show character m magicianScared at center',
		ruriri(73, 'Ach??? Ktoś tu jest?? Jakim cudem udało ci się wejść, i brama, i wieża są szczelnie zamknięte!'),
		'Y\'udeh zdradziła mi tajemne przejście, a strażnik otworzył drzwi. Proszę, posłuchaj co mam do powiedzenia.',
		ruriri(74, 'Znasz Yu? I skąd strażnik miałby mieć klucz, o czym ty mówisz? Kim jesteś i czego ode mnie chcesz?!'),
		'Sama zostawiłaś mi wiadomość. Mówiłaś do mnie z przyszłości. Coś bardzo złego się stanie, jeśli wypowiesz to zaklęcie.',
		'show character m magicianSad at center',
		ruriri(75, 'Coś bardzo złego?'),
		'Mówiłaś, że Czarnoksiężnik nie może zostać pozbawiony mocy, bo inaczej coś złego stanie się z ziemią i wszystkimi uczestnikami rytuału. Mówiąc to, płakałaś. Dlatego jestem tu, by powstrzymać się od wypowiedzenia zaklęcia. Sama mnie o to prosiłaś!',
		'show character m magicianAngry at center',
		ruriri(76, 'Ja... nie... niemożliwe! Jak miałabym do ciebie mówić, nigdy wcześniej cię nie widziałam!'),
		ruriri(77, 'Poza tym wszyscy wiedzą, że rytuał to tylko tradycja i nawet jeśli Czarnoksiężnik nie będzie w stanie go przeprowadzić, to nic się nie stanie.'),
		ruriri(78, 'Nareszcie nadszedł czas, żeby spotkało go zasłużone upokorzenie. I żeby wszyscy się dowiedzieli, jakim naprawdę jest potworem!'),
		ruriri(79, 'To nie może czekać. Już wszystko przygotowałam. To musi stać się dzisiaj podczas rytuału!'),
		'show character m magicianSad at center',
		ruriri(80, 'A jeśli ten rytuał, to rzeczywiście coś więcej?'),
		ruriri(81, 'Jeśli te trzęsienia ziemi nie były przypadkowe?'),
		ruriri(82, 'Jeśli to nie tylko wyczarowanie kilku fajerwerków?'),
		ruriri(83, 'Co, jeśli... jeśli nie będę w stanie sobie poradzić?'),
		'show character m magicianTriumphant at center',
		ruriri(84, 'To nawet lepiej! Wtedy to ja będę tym, kto powstrzyma katastrofę!'),
		ruriri(85, 'Wreszcie im pokażę... pokażę na co mnie stać!'),
		'Nie uda ci się. Już próbowałaś. Musiałaś cofnąć czas, starając się, żeby do tego nie doszło. Dlatego prosiłaś mnie, żeby cię powstrzymać!',
		'show character m magicianAngry at center',
		ruriri(86, 'Ty też we mnie nie wierzysz! Jak oni wszyscy, patrzą się na mnie wciąż z tym politowaniem, udają miłych, żeby potem, kiedy myślą, że nie słyszę, wspominać moją siostrę i żałować, że to ja przeżyłam, a nie ona!'), // wybucha
		'show character m magicianSad at center',
		ruriri(87, 'Myślisz, że to łatwe? Że sama, leżąc w nocy w łóżku, nie zastanawiam się, czy może mają rację i rzeczywiście to ona powinna przeżyć zamiast mnie?'),
		ruriri(88, 'Te myśli nie dają mi spokoju, nie pozwalają spać. Nie potrafię już patrzeć się im w twarz.'),
		ruriri(89, 'Dlatego ta wieża stała się moim domem, więzieniem, z którego nie mogę się wydostać. Jedynym sposobem, aby się zemścić, jest pokonać go jego własną bronią, jego ukochaną magią.'),
		'show character m magicianAngry at center',
		ruriri(90, 'Tyle lat ćwiczyłam, żeby wreszcie móc to zrobić. Żeby wreszcie dosięgnęła go sprawiedliwość.'),
		'show character m magicianTriumphant at center',
		ruriri(91, 'Ooo tak, cóż za słodki to będzie widok, zobaczyć przerażenie na tej jego starej, zgrzybiałej twarzy, zmyć z niej ten paskudny, wszechwiedzący uśmieszek.'),
		'show character m magicianSad at center',
		ruriri(92, 'Twarzy człowieka, który nie zawahał się wysłać na śmierć dziecka w imię swoich własnych interesów...'),
		ruriri(93, 'Pewnie nie chciał, żebym pałętała się mu pod nogami, dlatego wykorzystał dogodną okazję.'),
		'show character m magicianAngry at center',
		ruriri(94, 'A moja tragiczna śmierć miała jeszcze bardziej zmotywować siostrę do zostania najlepszą magiczką, którą widziała ta kraina.'),
		ruriri(95, 'Wiedział, że nie zdąży mnie dogonić, dlatego pozwolił jej iść.'),
		'show character m magicianSad at center',
		ruriri(96, 'Jednak w swoich obliczeniach nie przewidział, że – ha, o ironio – zgubię się i znalezienie szlaku zajmie mi tyle czasu co jej dotarcie do mnie.'),
		ruriri(97, 'Bezhet... nadal mogłabyś żyć, gdyby nie on. Gdyby tylko nie ten przeklęty, obrzydliwy starzec!'),
		'show character m magicianTriumphant at center',
		ruriri(98, 'Dlatego należy mu się kara. Czas zemsty nadchodzi.'),
		ruriri(99, 'Kiedy tylko ludzie z miasta dowiedzą się, co zaszło, na pewno postanowią go wypędzić.'),
		ruriri(100, 'A ja, czarodziejka, której udało się odprawić rytuał, zajmę jego miejsce.'),
		'Wiem, że to trudne, ale na pewno istnieje jakieś inne wyjście. Co jeśli oni wszyscy zginą? Czy wtedy sama nie staniesz się Czarnoksiężnikiem?',
		'show character m magicianSad at center',
		ruriri(101, 'Przestań, nie... nie mów tak! To, co robię, to jedyna słuszna droga. Za zbrodnię należy się kara.'),
		ruriri(102, 'On jest potworem, a ja... ja tylko pilnuję, żeby sprawiedliwości stało się zadość!'),
		'show character m magicianTriumphant at center',
		ruriri(103, 'Poza tym, jeśli cokolwiek się stanie, to ja ich obronię. Potrafię. Jestem czarodziejką zdolną zastąpić Czarnoksiężnika. Tyle ćwiczyłam...'),
		'A jeśli jednak nie dasz sobie rady?',
		'show character m magicianAngry at center',
		ruriri(104, 'Przestań! Mącisz mi w głowie... czy to on cię tu przysłał? Jesteś jego pachołkiem?'),
		ruriri(105, 'To dlatego udało ci się tu wejść! Teraz już wszystko rozumiem...'),
		'show character m magicianSad at center',
		ruriri(106, 'Ale czy w takim razie wie, co ma się tu stać? Nie, to niemożliwe. Jakoś by zareagował! A może tylko podejrzewa? Może obserwuje mnie przez cały ten czas?'),
		ruriri(107, 'Nie, nie, oddal od siebie te myśli, one tylko sprawiają, że stajesz się słabsza! Nieważne, czy wie, czy nie. Trudno. Kości zostały rzucone.'),
		'play sound party with loop',
		'show character m magicianScared at center',
		ruriri(108, 'Rytuał już zaraz się zacznie! Muszę się pospieszyć. To moja jedyna szansa.'),
		'show character m magicianSad at center',
		ruriri(109, 'Jeśli nic nie zrobię, znowu będę musiała czekać na kolejną szansę cały rok. Ile razy byłam już u progu i w końcu stchórzyłam...'),
		'show character m magicianTriumphant at center',
		'stop music with fade 1',
		ruriri(110, 'Jednak tym razem będzie inaczej. To nie czas na wątpliwości. Nie ma ani chwili do stracenia.'),
		// czarodziejka podświetlona na magicznie, będzie wypowiadać zaklęcie
		'show character m magicianAngry at center',
		ruriri(111, 'Popamiętasz, głupcze! Zobaczysz, na co mnie stać, biedną, słabą czarodziejkę, na którą przez tyle lat patrzyłeś z politowaniem!'),
		ruriri(112, 'Czas już podnieść kurtynę i powiedzieć wszystkim prawdę o twoich występkach!'),
		'show character m magicianSadGlow at center',
		'play music elegy with loop',
		ruriri(113, 'Nonhanipa uenigra, nonpono, saimor, acore, tettegrau. Utduli Alo utpe, tebstrirum. Utduli Alo utpe, tebstriru. Scruuosri quamhocbstri, cumlortis, ie teinccle.'),
		simpleChoice(['Użyj amuletu.'], ''),
	],
	'Użyj_amuletu.': [
		ruriri(114, 'Sewi flih, wofia, agifrizien, hel. Sewi flih, wofia, agifrizien, hel. Sewi flih, wofia, agifrizien, hel. Sewi flih, wofia, agifrizien, hel!'),
		'Jak to? Dlaczego nic się nie stało?',
		'jump Próba2',
	],
		'Próba2': [
		simpleChoice(['Ponownie użyj amuletu.'], ''),
	],
		'Ponownie_użyj_amuletu.': [
		'Nic się nie dzieje...',
		'Czyżby ten amulet był podróbką?!',
		'Poczekaj, Riruth! Nie rób tego!',
		'show character m magicianTriumphantGlow at center',
		ruriri(115, 'Niech sprawiedliwość wreszcie zatriumfuje! Niech potężny czarodziej utraci swą moc!'), // nadal wypowiadasz formułki magicznie, z pompą
		ruriri(116, 'O Wielka Matko, która wysłuchujesz naszych próśb, przenieś mnie na wzgórze! Sob dert, efrojar, avugius!'),
		'Czekaj, przemyśl najpierw, co zrobisz! A co, jeśli legendy były prawdziwe i zaraz pojawi się tam...',
		'hide character m with fadeOut',
		'play sound teleport',
		'wait 500',
		'stop sound party',
		'play sound demon',
		'...demon...',
		// TODO krzyk ludzi
		'Czyli legenda okazała się prawdą!',
		'Demon wydostał się z ziemi i nie ma tam nikogo, kto będzie w stanie go powstrzymać.',
		'Cholera! Ta głupia czarodziejka!',
		'Nie zdążę tam dotrzeć na czas – a nawet jeśli, co mi to da?',
		'Nie mam już eliksiru, a amulet okazał się bezużyteczny...',
		'Chociaż może tu uda mi się znaleźć coś przydatnego!',
		'Tyle tu słoików, któryś MUSI zadziałać!',
		'Ale niepodpisane są całkiem bezużyteczne, skąd mam wiedzieć, który może mi pomóc?',
		'Gdyby tylko była tu ze mną Y\'udeh...',
		'Mam nadzieję, że udało jej się namówić przynajmniej część ludzi do zostania w domu.',
		'Jeśli nie... nawet nie chcę myśleć, co tam się teraz dzieje.',
		'play sound teleport',
		'show character m magicianSad at center',
		ruriri(117, 'Nie dam sobie rady. Mają rację. Tak naprawdę jestem słaba. Myliłam się. Nie będę bohaterem, który wszystkich uratuje. Teraz wszyscy... ci wszyscy ludzie zginą, zostaną zamordowani, mimo że są niewinni... i to wszystko przeze mnie...'), // tak wiesz, bez ładu i składu, straciłaś nadzieję, martwo
		ruriri(118, 'A ja będę tylko siedzieć i patrzeć, bo co mogę zrobić?'),
		'show character m magicianCrying at center',
		ruriri(119, 'Niewinne dzieci... na moich oczach... ich krzyki, przeszywające moje ciało, ich zakrwawione twarze...'),
		ruriri(120, 'Zupełnie, jak tamtego dnia... Bezhet, nadal w snach widzę twoją siną twarz...'),
		'Riruth! Może jest jeszcze jakaś nadzieja!',
		'Musisz wziąć się w garść!',
		'Na pewno jest coś, co możesz zrobić, na pewno!',
		ruriri(121, 'Nie ma już nadziei... Czarnoksiężnik nie odzyska swojej mocy aż do wschodu słońca. Do tego czasu wszyscy już dawno będą martwi...'),
		'Do wschodu słońca...',
		simpleChoice(['Poproś Riruth o wysłanie wiadomości w przeszłość.'], ''),
		],
	'Poproś_Riruth_o_wysłanie_wiadomości_w_przeszłość.': [
		'No tak! Odpowiedź przecież narzuca się sama!',
		'To nie może być pierwszy raz, kiedy tu jestem.',
		'Czyli poprzednio też nie udało mi się jej przekonać...',
		'Riruth! Jest coś, co możesz zrobić!',
		'I jedynie ty możesz tego dokonać!',
		'Przemów do mnie z przeszłości. Opowiedz wszystko mojemu przeszłemu ja.',
		'Daj mu przy okazji coś, czym będzie mieć szansę cię powstrzymać.',
		'W ten sposób wciąż będzie szansa, aby nie dopuścić do tragedii!',
		ruriri(122, 'Przemówić do przeszłości...?'),
		'show character m magicianTriumphant at center',
		ruriri(123, 'Tak, to może się udać! Weź się w garść Riruth, jeszcze nie wszystko stracone!'),
		ruriri(124, 'Musisz tylko skupić się i przypomnieć sobie zaklęcie.'),
		'show character m magicianSad at center',
		ruriri(125, 'Duchy czasu, spoglądające na nas, zlitujcie się nad nami, biednymi śmiertelnikami, i otwórzcie nam wrota do przeszłości!'),
		'show character m magicianSadGlow at center',
		ruriri(126, 'Tily aflakius, ifyecar, onukri...'),
		'hide character m with fadeOut',
		'stop music with fade 1',
		'show scene black with fadeIn',
		'wait 2000',
		resetStorageStart2,
		'jump Start2',
	],
	'Zakończenie3': [
		'show scene potion_shop with fadeIn',
		//Zakończenie 3: Nie wymieniliśmy się z szurem, mamy początkowy eliksir
		'show scene potion_shop with fadeIn',
		'play music finalLabor with loop fade 1',
		'<em>W którejś z komnat słychać donośny męski głos, śpiewający balladę o królowej wróżek. Z pewnością nikt poza Czarnoksiężnikiem i Riruth tu nie przychodzi. Poza tym, że trochę zakurzone, to całkiem niezłe miejsce do ćwiczeń. Ma wspaniałe echo.</em>', // kursywą
		'Zagadka, po co był mu potrzebny dostęp do wieży, rozwiązana.',
		'Lepiej będzie, jeśli się pospieszę, sądząc po odgłosach, zostało już niewiele czasu.',
		'show character m magicianSad at center',
		'show character m magicianSad at center',
		ruriri(63, 'Tylko spokojnie. Oddychaj. Wszystko będzie dobrze.'),
		ruriri(64, 'Pokażesz im, na co cię stać.'),
		ruriri(65, 'Po tych wszystkich latach wreszcie nadszedł czas, kiedy ludzie poznają prawdę. Zobaczą, z czym musiałam żyć tyle lat. Zobaczą, że nie jestem tylko cieniem siostry.'),
		ruriri(66, 'Jeszcze raz: wypowiem zaklęcie ze strony 278, potem z 352... nie, nie, 351. W pokoju powinien rozbłysnąć różowy płomień, to oznacza, że klątwa zadziałała i stary dziad nie będzie mógł używać czarów aż do ponownego wschodu słońca.'),
		ruriri(67, 'Wtedy przeteleportuję się na wzgórze i opowiem wszystkim, jaki jest naprawdę ich ukochany, dobry Czarnoksiężnik.'),
		ruriri(68, 'A potem... a potem sama przeprowadzę rytuał.'),
		ruriri(69, 'Wszyscy tam będą... ich oczy wpatrzone prosto we mnie... wyczekujące każdego słowa...'),
		'show character m magicianAngry at center',
		ruriri(70, 'Potrafisz, Riru, potrafisz! Przez te wszystkie lata ćwiczyłaś właśnie dlatego momentu!'),
		ruriri(71, 'Poza tym to tylko wystrzelenie kilku płomieni, wypowiedzenie formułki i fajerwerki.'),
		ruriri(72, 'Bułka z masłem.'),
		'Przepraszam, że nachodzę tak znienacka. To pilna sprawa.',
		'show character m magicianScared at center',
		ruriri(73, 'Ach??? Ktoś tu jest?? Jakim cudem udało ci się wejść, i brama, i wieża są szczelnie zamknięte!'),
		'Y\'udeh zdradziła mi tajemne przejście, a strażnik otworzył drzwi. Proszę, posłuchaj co mam do powiedzenia.',
		ruriri(74, 'Znasz Yu? I skąd strażnik miałby mieć klucz, o czym ty mówisz? Kim jesteś i czego ode mnie chcesz?!'),
		'Sama zostawiłaś mi wiadomość. Mówiłaś do mnie z przyszłości. Coś bardzo złego się stanie, jeśli wypowiesz to zaklęcie.',
		'show character m magicianSad at center',
		ruriri(75, 'Coś bardzo złego?'),
		'Mówiłaś, że Czarnoksiężnik nie może zostać pozbawiony mocy, bo inaczej coś złego stanie się z ziemią i wszystkimi uczestnikami rytuału. Mówiąc to, płakałaś. Dlatego jestem tu, by powstrzymać się od wypowiedzenia zaklęcia. Sama mnie o to prosiłaś!',
		'show character m magicianAngry at center',
		ruriri(76, 'Ja... nie... niemożliwe! Jak miałabym do ciebie mówić, nigdy wcześniej cię nie widziałam!'),
		ruriri(77, 'Poza tym wszyscy wiedzą, że rytuał to tylko tradycja i nawet jeśli Czarnoksiężnik nie będzie w stanie go przeprowadzić, to nic się nie stanie.'),
		ruriri(78, 'Nareszcie nadszedł czas, żeby spotkało go zasłużone upokorzenie. I żeby wszyscy się dowiedzieli, jakim naprawdę jest potworem!'),
		ruriri(79, 'To nie może czekać. Już wszystko przygotowałam. To musi stać się dzisiaj podczas rytuału!'),
		'show character m magicianSad at center',
		ruriri(80, 'A jeśli ten rytuał, to rzeczywiście coś więcej?'),
		ruriri(81, 'Jeśli te trzęsienia ziemi nie były przypadkowe?'),
		ruriri(82, 'Jeśli to nie tylko wyczarowanie kilku fajerwerków?'),
		ruriri(83, 'Co, jeśli... jeśli nie będę w stanie sobie poradzić?'),
		'show character m magicianTriumphant at center',
		ruriri(84, 'To nawet lepiej! Wtedy to ja będę tym, kto powstrzyma katastrofę!'),
		ruriri(85, 'Wreszcie im pokażę... pokażę na co mnie stać!'),
		'Nie uda ci się. Już próbowałaś. Musiałaś cofnąć czas, starając się, żeby do tego nie doszło. Dlatego prosiłaś mnie, żeby cię powstrzymać!',
		'show character m magicianAngry at center',
		ruriri(86, 'Ty też we mnie nie wierzysz! Jak oni wszyscy, patrzą się na mnie wciąż z tym politowaniem, udają miłych, żeby potem, kiedy myślą, że nie słyszę, wspominać moją siostrę i żałować, że to ja przeżyłam, a nie ona!'), // wybucha
		'show character m magicianSad at center',
		ruriri(87, 'Myślisz, że to łatwe? Że sama, leżąc w nocy w łóżku, nie zastanawiam się, czy może mają rację i rzeczywiście to ona powinna przeżyć zamiast mnie?'),
		ruriri(88, 'Te myśli nie dają mi spokoju, nie pozwalają spać. Nie potrafię już patrzeć się im w twarz.'),
		ruriri(89, 'Dlatego ta wieża stała się moim domem, więzieniem, z którego nie mogę się wydostać. Jedynym sposobem, aby się zemścić, jest pokonać go jego własną bronią, jego ukochaną magią.'),
		'show character m magicianAngry at center',
		ruriri(90, 'Tyle lat ćwiczyłam, żeby wreszcie móc to zrobić. Żeby wreszcie dosięgnęła go sprawiedliwość.'),
		'show character m magicianTriumphant at center',
		ruriri(91, 'Ooo tak, cóż za słodki to będzie widok, zobaczyć przerażenie na tej jego starej, zgrzybiałej twarzy, zmyć z niej ten paskudny, wszechwiedzący uśmieszek.'),
		'show character m magicianSad at center',
		ruriri(92, 'Twarzy człowieka, który nie zawahał się wysłać na śmierć dziecka w imię swoich własnych interesów...'),
		ruriri(93, 'Pewnie nie chciał, żebym pałętała się mu pod nogami, dlatego wykorzystał dogodną okazję.'),
		'show character m magicianAngry at center',
		ruriri(94, 'A moja tragiczna śmierć miała jeszcze bardziej zmotywować siostrę do zostania najlepszą magiczką, którą widziała ta kraina.'),
		ruriri(95, 'Wiedział, że nie zdąży mnie dogonić, dlatego pozwolił jej iść.'),
		'show character m magicianSad at center',
		ruriri(96, 'Jednak w swoich obliczeniach nie przewidział, że – ha, o ironio – zgubię się i znalezienie szlaku zajmie mi tyle czasu co jej dotarcie do mnie.'),
		ruriri(97, 'Bezhet... nadal mogłabyś żyć, gdyby nie on. Gdyby tylko nie ten przeklęty, obrzydliwy starzec!'),
		'show character m magicianTriumphant at center',
		ruriri(98, 'Dlatego należy mu się kara. Czas zemsty nadchodzi.'),
		ruriri(99, 'Kiedy tylko ludzie z miasta dowiedzą się, co zaszło, na pewno postanowią go wypędzić.'),
		ruriri(100, 'A ja, czarodziejka, której udało się odprawić rytuał, zajmę jego miejsce.'),
		'Wiem, że to trudne, ale na pewno istnieje jakieś inne wyjście. Co jeśli oni wszyscy zginą? Czy wtedy sama nie staniesz się Czarnoksiężnikiem?',
		'show character m magicianSad at center',
		ruriri(101, 'Przestań, nie... nie mów tak! To, co robię, to jedyna słuszna droga. Za zbrodnię należy się kara.'),
		ruriri(102, 'On jest potworem, a ja... ja tylko pilnuję, żeby sprawiedliwości stało się zadość!'),
		'show character m magicianTriumphant at center',
		ruriri(103, 'Poza tym, jeśli cokolwiek się stanie, to ja ich obronię. Potrafię. Jestem czarodziejką zdolną zastąpić Czarnoksiężnika. Tyle ćwiczyłam...'),
		'A jeśli jednak nie dasz sobie rady?',
		'show character m magicianAngry at center',
		ruriri(104, 'Przestań! Mącisz mi w głowie... czy to on cię tu przysłał? Jesteś jego pachołkiem?'),
		ruriri(105, 'To dlatego udało ci się tu wejść! Teraz już wszystko rozumiem...'),
		'show character m magicianSad at center',
		ruriri(106, 'Ale czy w takim razie wie, co ma się tu stać? Nie, to niemożliwe. Jakoś by zareagował! A może tylko podejrzewa? Może obserwuje mnie przez cały ten czas?'),
		ruriri(107, 'Nie, nie, oddal od siebie te myśli, one tylko sprawiają, że stajesz się słabsza! Nieważne, czy wie, czy nie. Trudno. Kości zostały rzucone.'),
		'play sound party with loop',
		'show character m magicianScared at center',
		ruriri(108, 'Rytuał już zaraz się zacznie! Muszę się pospieszyć. To moja jedyna szansa.'),
		'show character m magicianSad at center',
		ruriri(109, 'Jeśli nic nie zrobię, znowu będę musiała czekać na kolejną szansę cały rok. Ile razy byłam już u progu i w końcu stchórzyłam...'),
		'show character m magicianTriumphant at center',
		'stop music with fade 1',
		ruriri(110, 'Jednak tym razem będzie inaczej. To nie czas na wątpliwości. Nie ma ani chwili do stracenia.'),
		// czarodziejka podświetlona na magicznie, będzie wypowiadać zaklęcie
		'show character m magicianAngry at center',
		ruriri(111, 'Popamiętasz, głupcze! Zobaczysz, na co mnie stać, biedną, słabą czarodziejkę, na którą przez tyle lat patrzyłeś z politowaniem!'),
		ruriri(112, 'Czas już podnieść kurtynę i powiedzieć wszystkim prawdę o twoich występkach!'),
		'show character m magicianSadGlow at center',
		'play music elegy with loop',
		ruriri(113, 'Nonhanipa uenigra, nonpono, saimor, acore, tettegrau. Utduli Alo utpe, tebstrirum. Utduli Alo utpe, tebstriru. Scruuosri quamhocbstri, cumlortis, ie teinccle.'),
		simpleChoice(['Użyj eliksiru.'], '1'),
	],
	'Użyj_eliksiru.1': [
		'stop music',
		'show character m magicianScared at center',
		ruriri(178, 'Aaaaaaa! To parzy! Co się dzieje? Nie mogę... złapać... tchu...'),
		'hide character m with fadeOut',
		'play sound bodyfall',
		'play sound ruriri_179 with loop', // ruriri(179, '...'),
		'Riruth!',
		'Muszę coś wymyślić.',
		'Eliksir był nawet mocniejszy niż Y\'udeh przewidziała.',
		'Jeśli czegoś nie zrobię, w tym tempie Riruth może tego nie przeżyć.',
		'Wygląda na to, że inni zostali uratowani, ale nie chcę, żeby to odbyło się...',
		'stop sound with fade 1',
		'play sound doorOpenSqueek',
		'play music finalLabor with loop fade 1',
		'w Jej kosztem? Dobrze zgaduję?',
		'w Rzeczywiście, zadanie zostało wypełnione, ale umówmy się, mogłoby ci pójść znacznie lepiej.',
		'w Oceniłbym to na 3 z plusem.',
		'w Oczywiście istnieje szansa, że twoim celem było jedynie przeklikanie się do samego końca, aby zobaczyć, co się stanie.',
		'w I żadna z żyjących w tym miasteczku osób nie obchodzi cię na tyle, by jeszcze raz przez to wszystko przechodzić, aż do skutku próbować doprowadzić do dobrego zakończenia.',
		'w Jeśli rzeczywiście tak jest – błąd leży z pewnością po mojej stronie.',
		'w Być może minęło za mało czasu, aby możliwe było żżycie się z którymkolwiek z mieszkańców.',
		'w Może powinienem był wezwać cię trochę wcześniej.',
		'w Ale nie przejmuj się, ostatnim, czego bym chciał, jest spowodowanie poczucia winy u kogoś, kto próbował nas ocalić.',
		'w Jeśli chcesz się teraz oddalić, zrozumiem.',
		'w Zresztą, wybacz mi te słowa, ale jest jeszcze wiele innych osób, gotowych zastąpić cię w tej misji.',
		'w O, czyżbym odzyskał twoją uwagę?',
		'w Oczywiście był to żart – mam nadzieję, że znasz się na żartach – nie bez powodu przecież wybrałem właśnie ciebie.',
		'w Wierzę, że jesteś w stanie sprawić, by wszyscy byli zadowoleni.',
		'w Dać nam prawdziwie dobre zakończenie.',
		'w Oczywiście, do niczego cię nie zmuszę.',
		'w Ale za to dam ci szansę.',
		'w Którą opcję wybierzesz?',
		'stop sound',
		'stop music with fade 3',
		simpleChoice(['Spróbuj jeszcze raz', 'Zakończ'], '1'),
	],
	'Spróbuj_jeszcze_raz1': [
		resetStorageStrzelnica4,
		'jump Strzelnica4'
	],
	'Zakończ1': [
		'w Naprawdę?',
		'w Ranisz mnie.',
		'w Wiesz, jak się namęczyłem, żeby cię tu ściągnąć? Ja, przepotężny Czarnoksiężnik?',
		'w Tak, to byłem ja przez cały ten czas!',
		'w Niezłe zaskoczenie, co?',
		'w I tak jak każdy porządny czarnoksiężnik, mam już swoje lata.',
		'w No dobrze, niby nic mnie nie boli, bo wszystko leczę czarami.',
		'w Ale to nie ma tu znaczenia! Liczy się szacunek do starszych! O tak, tak. Podstawa dobrze funkcjonującego społeczeństwa.',
		'w Dlatego proszę, przemyśl to raz jeszcze!',
		simpleChoice(['Spróbuj jeszcze raz', 'Zakończ'], '2'),
	],
	'Spróbuj_jeszcze_raz2': [
		resetStorageStrzelnica4,
		'jump Strzelnica4'
	],
	'Zakończ2': [
		'w Widzę, że moje argumenty cię nie przekonały.',
		'w To może przynajmniej zostaniesz tu chwilę, tak po prostu, żeby pogadać?',
		'w Nie?',
		'w Już tak naprawdę, naprawdę chcesz sobie iść?',
		'w Na pewno?',
		'w Definitywnie?',
		'w ...',
		'w No dobrze, nie ma w takim razie rady.',
		'w Idź i działaj, a ja zajmę się resztą!',
		'w Ale jeśli zmienisz zdanie, to wiesz, że zawsze możesz wrócić, prawda?',
		'end',]
});
