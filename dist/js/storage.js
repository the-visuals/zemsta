/* global monogatari */

// Persistent Storage Variable
monogatari.storage ({
	Karczma3visible: true,
	Strzelnica3visible: true,
	Piekarnia3visible: true,
	Wieża_magiczna3visible: true,
	Cela5visible: true,

	nonDilutedPotion: true,
	dilutedPotion: false,
	money: false,
	amulet: false
});