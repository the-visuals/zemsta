'use strict';

// The name of your game, no spaces or special characters.
const name = 'Monogatari';

// The version of the cache, changing this will force everything to be cached
// again.
const version = '0.1.0';

const files = [

	'/',

	// General Files
	'manifest.json',

	// Engine Files
	'engine/core/monogatari.css',
	'engine/core/monogatari.js',

	// HTML Files
	'index.html',

	// Style Sheets
	'style/main.css',

	// JavaScript Files
	'js/options.js',
	'js/storage.js',
	'js/script.js',
	'js/main.js',

	// App Images
	'favicon.ico',
	'assets/icons/icon_48x48.png',
	'assets/icons/icon_60x60.png',
	'assets/icons/icon_70x70.png',
	'assets/icons/icon_76x76.png',
	'assets/icons/icon_96x96.png',
	'assets/icons/icon_120x120.png',
	'assets/icons/icon_128x128.png',
	'assets/icons/icon_150x150.png',
	'assets/icons/icon_152x152.png',
	'assets/icons/icon_167x167.png',
	'assets/icons/icon_180x180.png',
	'assets/icons/icon_192x192.png',
	'assets/icons/icon_310x150.png',
	'assets/icons/icon_310x310.png',
	'assets/icons/icon_512x512.png',

	'assets/music/lute1.mp3',
	'assets/music/lute2.mp3',
	'assets/music/lute3.mp3',
	'assets/music/lute4.mp3',
	'assets/music/house.mp3',
	'assets/music/final-labor.mp3',
	'assets/music/bensound-birthofahero.mp3',
	'assets/music/Elegy - Wayne Jones.mp3',
	'assets/music/dungeon.mp3',

	'assets/voices/archerLady_01.mp3',
	'assets/voices/archerLady_36.mp3',
	'assets/voices/bakerLady_009.mp3',
	'assets/voices/bakerLady_044.mp3',
	'assets/voices/guard_028.mp3',
    'assets/voices/ruriri003.mp3',
    'assets/voices/ruriri089.mp3',
    'assets/voices/ruriri124.mp3',
	'assets/voices/archerLady_02.mp3',
	'assets/voices/archerLady_37.mp3',
	'assets/voices/bakerLady_010.mp3',
	'assets/voices/bakerLady_045.mp3',
	'assets/voices/guard_029.mp3',
    'assets/voices/ruriri004.mp3',
    'assets/voices/ruriri090.mp3',
    'assets/voices/ruriri125.mp3',
	'assets/voices/archerLady_03.mp3',
	'assets/voices/archerLady_38.mp3',
	'assets/voices/bakerLady_011.mp3',
	'assets/voices/bakerLady_046.mp3',
	'assets/voices/guard_030.mp3',
    'assets/voices/ruriri005.mp3',
    'assets/voices/ruriri091.mp3',
    'assets/voices/ruriri126.mp3',
	'assets/voices/archerLady_04.mp3',
	'assets/voices/archerLady_39.mp3',
	'assets/voices/bakerLady_012.mp3',
	'assets/voices/bakerLady_047.mp3',
	'assets/voices/guard_031.mp3',
    'assets/voices/ruriri006.mp3',
    'assets/voices/ruriri092.mp3',
    'assets/voices/ruriri178.mp3',
	'assets/voices/archerLady_05.mp3',
	'assets/voices/archerLady_40.mp3',
	'assets/voices/bakerLady_013.mp3',
	'assets/voices/bakerLady_048.mp3',
	'assets/voices/guard_032.mp3',
    'assets/voices/ruriri007.mp3',
    'assets/voices/ruriri093.mp3',
    'assets/voices/ruriri179.mp3',
	'assets/voices/archerLady_06.mp3',
	'assets/voices/archerLady_41.mp3',
	'assets/voices/bakerLady_014.mp3',
	'assets/voices/bakerLady_049.mp3',
	'assets/voices/guard_033.mp3',
    'assets/voices/ruriri008.mp3',
    'assets/voices/ruriri094.mp3',
    'assets/voices/szur_001.mp3',
	'assets/voices/archerLady_07.mp3',
	'assets/voices/archerLady_42.mp3',
	'assets/voices/bakerLady_015.mp3',
	'assets/voices/bakerLady_050.mp3',
	'assets/voices/guard_034.mp3',
    'assets/voices/ruriri009.mp3',
    'assets/voices/ruriri095.mp3',
    'assets/voices/szur_002.mp3',
	'assets/voices/archerLady_08.mp3',
	'assets/voices/archerLady_43.mp3',
	'assets/voices/bakerLady_016.mp3',
	'assets/voices/bakerLady_051.mp3',
	'assets/voices/guard_035.mp3',
    'assets/voices/ruriri061.mp3',
    'assets/voices/ruriri096.mp3',
    'assets/voices/szur_003.mp3',
	'assets/voices/archerLady_09.mp3',
	'assets/voices/archerLady_44.mp3',
	'assets/voices/bakerLady_017.mp3',
	'assets/voices/bakerLady_052.mp3',
	'assets/voices/guard_036.mp3',
    'assets/voices/ruriri062.mp3',
    'assets/voices/ruriri097.mp3',
    'assets/voices/szur_004.mp3',
	'assets/voices/archerLady_10.mp3',
	'assets/voices/archerLady_45.mp3',
	'assets/voices/bakerLady_018.mp3',
	'assets/voices/guard_001.mp3',
    'assets/voices/guard_037.mp3',
    'assets/voices/ruriri063.mp3',
    'assets/voices/ruriri098.mp3',
    'assets/voices/szur_005.mp3',
	'assets/voices/archerLady_11.mp3',
	'assets/voices/archerLady_46.mp3',
	'assets/voices/bakerLady_019.mp3',
	'assets/voices/guard_002.mp3',
    'assets/voices/guard_038.mp3',
    'assets/voices/ruriri064.mp3',
    'assets/voices/ruriri099.mp3',
    'assets/voices/szur_006.mp3',
	'assets/voices/archerLady_12.mp3',
	'assets/voices/archerLady_47.mp3',
	'assets/voices/bakerLady_020.mp3',
	'assets/voices/guard_003.mp3',
    'assets/voices/guard_039.mp3',
    'assets/voices/ruriri065.mp3',
    'assets/voices/ruriri100.mp3',
    'assets/voices/szur_007.mp3',
	'assets/voices/archerLady_13.mp3',
	'assets/voices/archerLady_48.mp3',
	'assets/voices/bakerLady_021.mp3',
	'assets/voices/guard_004.mp3',
    'assets/voices/guard_040.mp3',
    'assets/voices/ruriri066.mp3',
    'assets/voices/ruriri101.mp3',
    'assets/voices/szur_008.mp3',
	'assets/voices/archerLady_14.mp3',
	'assets/voices/archerLady_49.mp3',
	'assets/voices/bakerLady_022.mp3',
	'assets/voices/guard_005.mp3',
    'assets/voices/guard_041.mp3',
    'assets/voices/ruriri067.mp3',
    'assets/voices/ruriri102.mp3',
    'assets/voices/szur_009.mp3',
	'assets/voices/archerLady_15.mp3',
	'assets/voices/archerLady_50.mp3',
	'assets/voices/bakerLady_023.mp3',
	'assets/voices/guard_006.mp3',
    'assets/voices/guard_042.mp3',
    'assets/voices/ruriri068.mp3',
    'assets/voices/ruriri103.mp3',
    'assets/voices/szur_010.mp3',
	'assets/voices/archerLady_16.mp3',
	'assets/voices/archerLady_51.mp3',
	'assets/voices/bakerLady_024.mp3',
	'assets/voices/guard_007.mp3',
    'assets/voices/guard_043.mp3',
    'assets/voices/ruriri069.mp3',
    'assets/voices/ruriri104.mp3',
    'assets/voices/szur_011.mp3',
	'assets/voices/archerLady_17.mp3',
	'assets/voices/archerLady_52.mp3',
	'assets/voices/bakerLady_025.mp3',
	'assets/voices/guard_008.mp3',
    'assets/voices/guard_044.mp3',
    'assets/voices/ruriri070.mp3',
    'assets/voices/ruriri105.mp3',
    'assets/voices/szur_012.mp3',
	'assets/voices/archerLady_18.mp3',
	'assets/voices/archerLady_53.mp3',
	'assets/voices/bakerLady_026.mp3',
	'assets/voices/guard_009.mp3',
    'assets/voices/guard_045.mp3',
    'assets/voices/ruriri071.mp3',
    'assets/voices/ruriri106.mp3',
    'assets/voices/szur_013.mp3',
	'assets/voices/archerLady_19.mp3',
	'assets/voices/archerLady_54.mp3',
	'assets/voices/bakerLady_027.mp3',
	'assets/voices/guard_010.mp3',
    'assets/voices/guard_046.mp3',
    'assets/voices/ruriri072.mp3',
    'assets/voices/ruriri107.mp3',
    'assets/voices/szur_014.mp3',
	'assets/voices/archerLady_20.mp3',
	'assets/voices/archerLady_55.mp3',
	'assets/voices/bakerLady_028.mp3',
	'assets/voices/guard_011.mp3',
    'assets/voices/guard_047.mp3',
    'assets/voices/ruriri073.mp3',
    'assets/voices/ruriri108.mp3',
    'assets/voices/szur_015.mp3',
	'assets/voices/archerLady_21.mp3',
	'assets/voices/archerLady_56.mp3',
	'assets/voices/bakerLady_029.mp3',
	'assets/voices/guard_012.mp3',
    'assets/voices/guard_048.mp3',
    'assets/voices/ruriri074.mp3',
    'assets/voices/ruriri109.mp3',
    'assets/voices/szur_016.mp3',
	'assets/voices/archerLady_22.mp3',
	'assets/voices/archerLady_57.mp3',
	'assets/voices/bakerLady_030.mp3',
	'assets/voices/guard_013.mp3',
    'assets/voices/guard_049.mp3',
    'assets/voices/ruriri075.mp3',
    'assets/voices/ruriri110.mp3',
    'assets/voices/szur_017.mp3',
	'assets/voices/archerLady_23.mp3',
	'assets/voices/archerLady_58.mp3',
	'assets/voices/bakerLady_031.mp3',
	'assets/voices/guard_014.mp3',
    'assets/voices/guard_050.mp3',
    'assets/voices/ruriri076.mp3',
    'assets/voices/ruriri111.mp3',
    'assets/voices/szur_018.mp3',
	'assets/voices/archerLady_24.mp3',
	'assets/voices/archerLady_59.mp3',
	'assets/voices/bakerLady_032.mp3',
	'assets/voices/guard_015.mp3',
    'assets/voices/guard_051.mp3',
    'assets/voices/ruriri077.mp3',
    'assets/voices/ruriri112.mp3',
    'assets/voices/szur_019.mp3',
	'assets/voices/archerLady_25.mp3',
	'assets/voices/archerLady_60.mp3',
	'assets/voices/bakerLady_033.mp3',
	'assets/voices/guard_017.mp3',
    'assets/voices/guard_052.mp3',
    'assets/voices/ruriri078.mp3',
    'assets/voices/ruriri113.mp3',
    'assets/voices/szur_020.mp3',
	'assets/voices/archerLady_26.mp3',
	'assets/voices/archerLady_61.mp3',
	'assets/voices/bakerLady_034.mp3',
	'assets/voices/guard_018.mp3',
    'assets/voices/guard_053.mp3',
    'assets/voices/ruriri079.mp3',
    'assets/voices/ruriri114.mp3',
    'assets/voices/szur_021.mp3',
	'assets/voices/archerLady_27.mp3',
	'assets/voices/archerLady_62.mp3',
	'assets/voices/bakerLady_035.mp3',
	'assets/voices/guard_019.mp3',
    'assets/voices/guard_054.mp3',
    'assets/voices/ruriri080.mp3',
    'assets/voices/ruriri115.mp3',
    'assets/voices/szur_022.mp3',
	'assets/voices/archerLady_28.mp3',
	'assets/voices/bakerLady_001.mp3',
	'assets/voices/bakerLady_036.mp3',
	'assets/voices/guard_020.mp3',
    'assets/voices/guard_055.mp3',
    'assets/voices/ruriri081.mp3',
    'assets/voices/ruriri116.mp3',
    'assets/voices/szur_023.mp3',
	'assets/voices/archerLady_29.mp3',
	'assets/voices/bakerLady_002.mp3',
	'assets/voices/bakerLady_037.mp3',
	'assets/voices/guard_021.mp3',
    'assets/voices/guard_056.mp3',
    'assets/voices/ruriri082.mp3',
    'assets/voices/ruriri117.mp3',
    'assets/voices/szur_024.mp3',
	'assets/voices/archerLady_30.mp3',
	'assets/voices/bakerLady_003.mp3',
	'assets/voices/bakerLady_038.mp3',
	'assets/voices/guard_022.mp3',
    'assets/voices/karczma1.mp3',
     'assets/voices/ruriri083.mp3',
    'assets/voices/ruriri118.mp3',
    'assets/voices/szur_025.mp3',
	'assets/voices/archerLady_31.mp3',
	'assets/voices/bakerLady_004.mp3',
	'assets/voices/bakerLady_039.mp3',
	'assets/voices/guard_023.mp3',
    'assets/voices/karczma2.mp3',
     'assets/voices/ruriri084.mp3',
    'assets/voices/ruriri119.mp3',
    'assets/voices/szur_028.mp3',
	'assets/voices/archerLady_32.mp3',
	'assets/voices/bakerLady_005.mp3',
	'assets/voices/bakerLady_040.mp3',
	'assets/voices/guard_024.mp3',
    'assets/voices/karczma3.mp3',
     'assets/voices/ruriri085.mp3',
    'assets/voices/ruriri120.mp3',
    'assets/voices/szur_029.mp3',
	'assets/voices/archerLady_33.mp3',
	'assets/voices/bakerLady_006.mp3',
	'assets/voices/bakerLady_041.mp3',
	'assets/voices/guard_025.mp3',
    'assets/voices/karczma4.mp3',
     'assets/voices/ruriri086.mp3',
    'assets/voices/ruriri121.mp3',
	'assets/voices/archerLady_34.mp3',
	'assets/voices/bakerLady_007.mp3',
	'assets/voices/bakerLady_042.mp3',
	'assets/voices/guard_026.mp3',
    'assets/voices/ruriri001.mp3',
    'assets/voices/ruriri087.mp3',
    'assets/voices/ruriri122.mp3',
	'assets/voices/archerLady_35.mp3',
	'assets/voices/bakerLady_008.mp3',
	'assets/voices/bakerLady_043.mp3',
	'assets/voices/guard_027.mp3',
    'assets/voices/ruriri002.mp3',
    'assets/voices/ruriri088.mp3',
    'assets/voices/ruriri123.mp3',


];

self.addEventListener ('install', (event) => {
	self.skipWaiting ();
	event.waitUntil (
		caches.open (`${name}-v${version}`).then ((cache) => {
			return cache.addAll (files);
		})
	);
});

self.addEventListener ('activate', (event) => {
	event.waitUntil (
		caches.keys ().then ((keyList) => {
			return Promise.all (keyList.map ((key) => {
				if (key !== `${name}-v${version}`) {
					return caches.delete (key);
				}
			}));
		})
	);
	return self.clients.claim ();
});

self.addEventListener ('fetch', (event) => {

	if (event.request.method !== 'GET') {
		return;
	}

	event.respondWith (
		caches.match (event.request).then ((cached) => {
			function fetchedFromNetwork (response) {
				const cacheCopy = response.clone ();

				caches.open (`${name}-v${version}`).then (function add (cache) {
					cache.put (event.request, cacheCopy);
				});
				return response;
			}

			function unableToResolve () {
				return new Response (`
					<!DOCTYPE html><html lang=en><title>Bad Request</title><meta charset=UTF-8><meta content="width=device-width,initial-scale=1"name=viewport><style>body,html{width:100%;height:100%}body{text-align:center;color:#545454;margin:0;display:flex;justify-content:center;align-items:center;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Fira Sans","Droid Sans","Helvetica Neue",sans-serif}h1,h2{font-weight:lighter}h1{font-size:4em}h2{font-size:2em}</style><div><h1>Service Unavailable</h1><h2>Sorry, the server is currently unavailable or under maintenance, try again later.</h2></div>
				`, {
					status: 503,
					statusText: 'Service Unavailable',
					headers: new Headers ({
						'Content-Type': 'text/html'
					})
				});
			}

			const networked = fetch (event.request)
				.then (fetchedFromNetwork, unableToResolve)
				.catch (unableToResolve);

			return cached || networked;
		})
	);
});